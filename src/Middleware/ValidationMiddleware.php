<?php
namespace App\Middleware;

use Respect\Validation\Exceptions\ValidationException;
use Respect\Validation\Validator as v;

/*

Este Middleware se encarga de automatizar las validaciones segun las reglas
definidas en la ruta usando los validadores de Respect\Validation

Ejemplo de uso:

// container, nombreCampo, validaciones, min, max, nombreAMostrar, modificadores
new V($container,[['materias', ['intVal'], [1, 5], 'materias array', ['each']],]));

// container, nombreCampo
new V($container,[['movilidad',
// reglas anidadas
[['clave', ['alnum'], [4, 10], 'clave property inside movilidad arrays'],
['campus', ['intVal'], [1, 11], 'campus property inside movilidad arrays'],]
// validacion de longitud no es aplicable en este caso
, null,
// nombre a mostrar cuando haya un error
'movilidad array', ['each', 'optional']],]));

Los modificadores para las reglas que no tienen anidaciones son not, each y optional,
las que contienen otras reglas solo pueden tener each y optional

Una regla que tiene otras anidadas debe tener null, en el tercer elemnto representa
una validacion de longitud que no tiene caso evaluar dado que la validacion se aplica
sobre sus elementos anidados.

Cuando una regla tiene otras anidadas y tiene each significa que evaluara las reglas
para cada uno de los elementos que contiene (se entiende que es un arreglo), cuando
no tiene el modificador each se entiende que son las posiciones del arreglo asociativo
(u objeto en el caso que evalue un json parseado).

Se pueden anidar indefinidamente las reglas siguiendo estos mismos principios.

Considerar que hay un bug con Respect\Validation, cuando hay un error de
validacion con las validaciones tipo v::key (contenidas en un keyset)
la validacion arrojara algo similar a esto:

"movilidad array (movilidad)" must be an array"
"Each item in "movilidad array (movilidad)" must be valid"

Aunque se haya enviado efectivamente un array, se marcara como un error
de keyset si en alguna de las reglas tipo v::key() contenidas hay algun
error en la validacion, al corregir la regla que da problemas ninguno
de los dos errores apareceran.

 */

final class ValidationMiddleware {

	private $container, $rules;

	public function __construct($container, $rules) {
		$this->container = $container;
		$this->rules = $rules;
	}

	private static function validaLongitud(&$validator, $rule) {

		$lengthRule = $rule[2];

		if (count($lengthRule) == 2) {
			/*
			v::length(int $min, int $max)
			v::length(int $min, null)
			v::length(null, int $max)
			 */
			$validator->length($lengthRule[0], $lengthRule[1]);
		} else {
			// v::length(int $min, int $max, boolean $inclusive = true)
			$validator->length($lengthRule[0], $lengthRule[1], true);
		}

	}

	private static function setChainedValidators(&$validator, $rule, $nested = false) {

		$numChainedValidations = count($rule[1]);

		// si en lugar de validadores hay reglas anidadas
		if (is_array($rule[1][0])) {
			self::evaluateNested($rule, $validator);
		} else {
			// la primera validacion es estatica
			// https://secure.php.net/manual/en/migration70.incompatible.php#migration70.incompatible.variable-handling.indirect
			$validator = v::{$rule[1][0]}();

			// validaciones encadenadas
			for ($i = 1; $i < $numChainedValidations; $i++) {
				$validator->{$rule[1][$i]}();
			}

			// se debe empezar por las reglas mas especificas
			self::validaLongitud($validator, $rule);

			if (isset($rule[4])) {

				$modifier = $rule[4];

				if (in_array('not', $modifier)) {
					$validator = v::not($validator);
				}

				if (in_array('each', $modifier)) {
					$validator = v::arrayVal()->each($validator);
				}

				// y se debe de terminar con las reglas mas generales o envolventes
				if (in_array('optional', $modifier) && !$nested) {
					$validator = v::optional($validator);
				}

			}

		}

	}

	private static function evaluateNested(&$validator, $rule) {

		$modifiersForNestedRules = [];
		$keySet = [];

		if (isset($rule[4])) {
			$modifiersForNestedRules = $rule[4];
		}

		foreach ($rule[1] as $key => $nestedRule) {

			$mandatory = true;

			self::setChainedValidators($validator, $nestedRule, true);

			if (isset($nestedRule[4]) && in_array('optional', $nestedRule[4])) {

				$mandatory = false;

			}

			$keySet[] = v::key($nestedRule[0], $validator, $mandatory);

		}

		// invocacin con numero de argumentos variables solo en PHP 5.6+
		$validator = v::keySet(...$keySet);

		if (in_array('each', $modifiersForNestedRules)) {
			$validator = v::arrayVal()->each($validator);
		}

		if (in_array('optional', $modifiersForNestedRules)) {
			$validator = v::optional($validator);
		}

	}

	private static function evaluateAll($fields, $rules) {

		$exceptions = [];
		$compareEach = false;
		$keySet = [];
		$timesEach = 0;
		$result = true;
		$validator = null; // mantenemos la referencia

		// regla por campo (solo si se definio)
		foreach ($rules as $key => $rule) {

			// si hay reglas anidadas
			if (is_array($rule[1][0])) {

				self::evaluateNested($validator, $rule);

			} else {

				self::setChainedValidators($validator, $rule);

			}

			$value = isset($fields[$rule[0]]) ? $fields[$rule[0]] : '';

			$result = $validator->validate($value);

			if (!$result) {
				try {

					$validator->assert('' . $rule[3] . ' (' . $rule[0] . ')');

				} catch (ValidationException $e) {

					$e->setName($rule[3]);
					$exceptions[$rule[0]] = $e->getMessages();
				}
			}

		}

		return $exceptions;

	}

	public function __invoke($request, $response, $next) {
		$params = $request->getParsedBody();
		$params = $params ? $params : [];
		$route = $request->getAttribute('route');
		$params = array_merge($params, $route->getArguments());
		$exceptions = self::evaluateAll($params, $this->rules);

		if (count($exceptions)) {
			return $this->container->responseFormatter->getJsonResponse($response, 400, [], 'Error(es) de validación.', $exceptions);
		} else {
			return $next($request, $response);
		}
	}

}
