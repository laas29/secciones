<?php
namespace App\Middleware;

/*
https://www.slimframework.com/docs/cookbook/enable-cors.html
https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS#Access-Control-Allow-Origin
https://www.html5rocks.com/static/images/cors_server_flowchart.png
 */

class CORSMiddleware
{

    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function __invoke($request, $response, $next)
    {
        $requestMethod = $request->getMethod();
        $route = $request->getAttribute("route");
        $router = $this->container->get('router');

        $methods = [];

        if (!empty($route)) {
            $pattern = $route->getPattern();

            foreach ($router->getRoutes() as $route) {
                if ($pattern === $route->getPattern()) {
                    $methods = array_merge_recursive($methods, $route->getMethods());
                }
            }
            //Methods holds all of the HTTP Verbs that a particular route handles.
        } else {
            $methods[] = $requestMethod;
        }

        // Estas cabeceras permiten hacer peticiones xhr con dominios diferentes
        $response = $response->withHeader('Access-Control-Allow-Origin', $_ENV['CORS_CLIENT'])
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader("Access-Control-Allow-Methods", implode(",", $methods));
        //  ->withHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");

        // OPTIONS no devuelve un cuerpo en la respuesta, solo cabeceeras con informacion de los verbos HTTP permitidos
        // en la ruta, asi que podemos retornar la peticion sin pasar por controlador
        if ($requestMethod == 'OPTIONS') {
            return $response->withStatus(200);
        }

        return $next($request, $response);

    }
}
