<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
	$settings = $c->get('settings')['renderer'];
	return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
	$settings = $c->get('settings')['logger'];
	$logger = new Monolog\Logger($settings['name']);
	$logger->pushProcessor(new Monolog\Processor\UidProcessor());
	$logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
	return $logger;
};

$container['pdo'] = function ($c) {
	$settings = $c->get('settings')['pdo'];
	return new PDO($settings['dsn'], $settings['user'], $settings['psw']);
};

$container[App\Controller\ScraperController::class] = function ($c) {
	return new App\Controller\ScraperController($c);
};

$container['responseFormatter'] = function ($c) {
	return new App\Helper\ResponseFormatterHelper();
};

$container['jwtHelper'] = function ($c) {
	return new App\Helper\JWTTokenHelper($c);
};

$container['ACLHelper'] = function ($c) {
	return new App\Helper\ACLHelper($c);
};
