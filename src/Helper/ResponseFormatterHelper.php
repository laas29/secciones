<?php
namespace App\Helper;

use Psr\Http\Message\ResponseInterface as Response;

final class ResponseFormatterHelper {

	public function __construct() {

	}

	public function getJsonResponse(Response $response, $httpStatus = 200, $data = array(), $debugInfo = "", $debugData = array()) {

		return $response->withJson(
			[
				'result' => $data,
				'debugInfo' => $debugInfo,
				'debugData' => $debugData,
			],
			$httpStatus
		);

	}

}
