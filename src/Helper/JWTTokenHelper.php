<?php
namespace App\Helper;

use \Firebase\JWT\JWT;

final class JWTTokenHelper {

	public function __construct($container) {

		$this->jwt_settings = $container->get('settings')['jwt'];
	}

	/**
	 * @param $user_defined_payload
	 * @return mixed
	 */
	public function createToken($user_defined_payload) {

		// implementacion JWT
		$now = new \DateTime();
		$future = new \DateTime('now +' . $this->jwt_settings['session_life']);
		$fixed_payload = [
			'iat' => $now->getTimeStamp(),
			'exp' => $future->getTimeStamp(),
			'iss' => $this->jwt_settings['issuer'],
			'aud' => $this->jwt_settings['audience'],
			// 'sub' => $credentials['codigo'],
		];
		$payload = array_merge($user_defined_payload, $fixed_payload);
		$secret = $this->jwt_settings['secret'];
		$token = JWT::encode($payload, $secret, "HS256");

		return $token;

	}

}
