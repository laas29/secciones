<?php
namespace App\Helper;

use Psr\Http\Message\ServerRequestInterface as Request;

final class ACLHelper {

	private $permissions = [
		'admin' => [

			/*Endpoints admin*/
			'admin.save.student',
			'admin.remove.student',
			'get.resource.student',
			'admin.save.program',
			'admin.remove.program',
			'get.program',
			'admin.save.subject',
			'admin.remove.subject',
			'get.subject',
			'admin.save.cycle',
			'admin.remove.cycle',
			'get.cycle',
			'admin.save.center',
			'admin.remove.center',
			'get.center',
			'admin.remove.preregistration',
			'admin.save.departament',
			'get.departament',
			'admin.save.user',
			'admin.remove.users',
			'get.user',
			'scraper',

			/*Endpoints user*/
			'recover.user.password',
			'update.user',
			'update.user.password',
			'get.userByEmail',
			'create.user',
			'check.user',
			'generate.reports.byProgram',
			'generate.reports.preregistredStudent',
			'generate.reports.preregistredStudent.BySubject',
			'generate.mobility.reports.preregistredStudent.BySubject',
			'generate.reports.allStudents',
			'generate.reports.allPrograms',
			'generate.reports.allPrograms',
			'generate.reports.allSubjects',
			'generate.reports.programsBySubject',
			'generate.reports.allCycles',
			'generate.reports.allDepartments',
			'generate.reports.allUsers',
			'generate.reports.allPreregisterCycles',

			/*Endpoints alumno*/
			'update.student',
			'update.student.password',
			'get.studentByCode',
			'student.createPreregistration',
			'student.preregistration.getAllSubjects',
			'student.getProgramSubjects',

		],
		'user' => [

			/*Endpoints user*/
			'recover.user.password',
			'update.user',
			'update.user.password',
			'get.userByEmail',
			'create.user',
			'check.user',
			'generate.reports.byProgram',
			'generate.reports.preregistredStudent',
			'generate.reports.preregistredStudent.BySubject',
			'generate.mobility.reports.preregistredStudent.BySubject',
			'generate.reports.allStudents',
			'generate.reports.allPrograms',
			'generate.reports.allPrograms',
			'generate.reports.allSubjects',
			'generate.reports.programsBySubject',
			'generate.reports.allCycles',
			'generate.reports.allDepartments',
			'generate.reports.allUsers',
			'generate.reports.allPreregisterCycles',

			/*Endpoints alumno*/
			'update.student',
			'update.student.password',
			'get.studentByCode',
			'student.createPreregistration',
			'student.preregistration.getAllSubjects',
			'student.getProgramSubjects',

		],
		'alumno' => [
			'update.student',
			'update.student.password',
			'get.studentByCode',
			'student.createPreregistration',
			'student.preregistration.getAllSubjects',
			'student.getProgramSubjects',
		],
	];

	/**
	 * @param $container
	 */
	public function __construct($container) {

		$this->jwt_settings = $container->get('settings')['jwt'];
		$this->c = $container;
	}

	/**
	 * @param $role string with the name of the role
	 * @param $request Psr\Http\Message\ServerRequestInterface object
	 */
	public function checkACLPermissions(Request $request, $role) {

		// $routeInfo = $request->getAttribute('routeInfo');
		$route = $request->getAttribute('route');
		// $arguments = $route->getArguments();
		// print_r($route->getName());
		// print_r($routeInfo);
		// print_r($request->getAttribute('routeInfo'));

		$found = array_search($route->getName(), $this->permissions['admin']);

		return $found !== false;

	}

}
