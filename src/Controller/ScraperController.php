<?php
namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class ScraperController
{

    public function __construct($container)
    {

        $this->container = $container;
        $this->db = new \App\Model\ScraperModel($container);

    }

    public function curle($params)
    {

        // crea nuevo recurso cURL
        $curl = curl_init();

        $options = array(
            CURLOPT_URL => "http://siiauescolar.siiau.udg.mx/wse/scpcata.cataxcarr",
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_RETURNTRANSFER => 1,
        );

        curl_setopt_array($curl, $options);
        $data = split("\n", curl_exec($curl));
        // $data = preg_split("/\n/", $curl_data);

        if (curl_errno($curl) == 81) {
            sleep(0.3);
            $data = curle($params);
        } else {
            curl_close($curl);
        }

        return $data;

    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $id
     * @return mixed
     */
    public function executeCURL(Request $request, Response $response, $id)
    {

        die();

        $programs = $this->db->getAllPrograms();

        echo '<pre>';

        foreach ($programs as $key => $value) {

            $params = array(
                "carrerap" => /*"COM", //*/$value["clave"],
                //"carrerap" => 'MEL',
                "ordenp" => "1",
                "mostrarp" => "5",
                "tipop" => "D",
            );

            echo print_r($params, true) . '<br>';

            $data = $this->curle($params);

            $length = count($data);

            // 238 lineas de basura html que imprime el csv de siiau
            for ($i = 238; $i < $length; $i++) {

                // $row = split(",", $data[$i]);
                preg_match_all('/\\"(.*?)\\"|\,(\d+)/', $data[$i], $row);

                echo print_r($row[0], true);

                if (count($row[0]) > 1) {

                    $subject = array(
                        ':subj' => $row[1][0],
                        ':clave' => $row[1][1],
                        ':nombre' => $row[1][2],
                        ':creditos' => $row[2][3],
                        ':carga_hor_teo' => $row[2][4],
                        ':carga_hor_pra' => $row[2][5],
                        ':tipo' => trim($row[1][6]),
                        ':nivel' => trim($row[1][7]),
                    );

                    $this->db->setSubject($subject);

                    /*
                    try {

                    } catch (PDOException $e) {

                    echo 'Error:' . $e->errorInfo() . '<br>';

                    die();

                    }
                     */

                    $lastInsertedId = $this->db->getLastInsertedId();

                    if ($lastInsertedId == 0) {

                        echo 'materia repetida ' . $subject[':clave'] . '<br>';

                        $createdSubject = $this->db->getSubject(array(':clave' => '%' . $subject[':clave'] . '%'));
                        echo print_r($createdSubject, true);
                        $lastInsertedId = $createdSubject[0]['id_materia'];

                    }

                    $this->db->setProgramSubject(
                        array(
                            ':id_programa' => /*1, //*/$value['id_programa'],
                            ':id_materia' => $lastInsertedId,
                        )
                    );

                }

                $data[$i] = $row;
            }

            echo '</pre>';

        }
        // foreach

        $body = $response->getBody();

        //$body->write($<curl_data></curl_data>);

        //$body->write("<pre>" . print_r($data, true) . "</pre>");

        return $response;

    }
}
