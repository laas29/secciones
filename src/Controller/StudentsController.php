<?php
namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class StudentsController {

	/**
	 * @param $container
	 */
	public function __construct($container) {

		$this->container = $container;
		$this->studentsModel = new \App\Model\StudentsModel($container);
		$this->respFormatter = $container->get('responseFormatter');

	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param $args
	 * @return mixed
	 */
	public function getStudent(Request $request, Response $response, $args) {

		$codigo = $args['codigo'];

		if ($this->container->jwtSession->role == 'alumno') {

			if ($codigo != $this->container->jwtSession->codigo) {

				return $this->respFormatter->getJsonResponse($response, 400, [], 'No tienes permisos para acceder a información de otro alumno.');

			}

		}

		$results = $this->studentsModel->getStudentCredentials($codigo);

		if (count($results)) {
			unset($results[0]['contrasena']);
		}

		return $this->respFormatter->getJsonResponse($response, 200, $results);

	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param $args
	 */
	public function updateStudent(Request $request, Response $response, $args) {

		$id_alumno = $args['id_alumno'];

		if ($this->container->jwtSession->role == 'alumno') {

			if ($id_alumno != $this->container->jwtSession->idAlumno) {

				return $this->respFormatter->getJsonResponse($response, 400, [], 'No tienes permisos para modificar la información de otro alumno.');

			}

		}

		$student = $request->getParsedBody();
		$row = array(
			':id_alumno' => $this->container->jwtSession->idAlumno,
			':nombre' => $student['nombre'],
			':apellidos' => $student['apellidos'],
			':codigo' => $student['codigo'],
			':email' => $student['email'],
			':id_programa' => $student['id_programa'],
			':id_generacion' => $student['id_ciclo'],
		);

		if ($this->container->jwtSession->codigo != $student['codigo']) {

			$existingStudent = $this->studentsModel->studentExists($student['codigo']);

			if (count($existingStudent)) {
				return $this->respFormatter->getJsonResponse($response, 400, [], 'El codigo que ha definido lo tiene asignado otro estudiante, si cree que esto es un error acuda con su coordinador.');
			}

		}

		$this->studentsModel->updateStudent($row);

		$payload = [
			"role" => $this->container->jwtSession->role,
			"email" => $student['email'],
			'carrera' => $student['id_programa'],
			'nombre' => $student['nombre'],
			'apellidos' => $student['apellidos'],
			'codigo' => $student['codigo'],
			'idAlumno' => $this->container->jwtSession->idAlumno,
			'cicloActivo' => $this->container->jwtSession->cicloActivo,
		];

		$token = $this->container->jwtHelper->createToken($payload);

		$data = [
			'token' => $token,
		];

		return $this->respFormatter->getJsonResponse($response, 200, [$data]);

	}

	public function updateStudentPassword(Request $request, Response $response, $args) {

		$id_alumno = $args['id_alumno'];

		if ($this->container->jwtSession->role == 'alumno') {

			if ($id_alumno != $this->container->jwtSession->idAlumno) {

				return $this->respFormatter->getJsonResponse($response, 400, [], 'No tienes permisos para modificar la información de otro alumno.');

			}

		}

		$student = $request->getParsedBody();
		$credentials = $this->studentsModel->getStudentCredentials($this->container->jwtSession->codigo)[0];

		if (password_verify($student['vieja_contrasena'], $credentials['contrasena'])) {

			$row = array(
				':id_alumno' => $this->container->jwtSession->idAlumno,
				':nueva_contrasena' => password_hash($student['nueva_contrasena'], PASSWORD_DEFAULT),
			);

			$this->studentsModel->updatePassword($row);

		} else {
			return $this->respFormatter->getJsonResponse($response, 400, [], 'La la vieja contraseña no coincide con la proporcionada');
		}

		return $this->respFormatter->getJsonResponse($response, 200, []);

	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param $args
	 * @return mixed
	 */
	public function createStudent(Request $request, Response $response, $args) {

		$student = $request->getParsedBody();

		$row = array(
			':nombre' => $student['nombre'],
			':apellidos' => $student['apellidos'],
			':codigo' => $student['codigo'],
			':correo' => $student['correo'],
			':contrasena' => password_hash($student['contrasena'], PASSWORD_DEFAULT),
			':id_programa' => $student['id_programa'],
			':id_generacion' => $student['id_ciclo'],
		);

		$this->studentsModel->createStudent($row);
		$lastInsertedId = $this->studentsModel->getLastInsertedId();

		if ($lastInsertedId > 0) {
			return $this->respFormatter->getJsonResponse($response, 201, ['id' => $lastInsertedId]);
		} else {
			return $this->respFormatter->getJsonResponse($response, 400, [], 'Hubo un error al crear al alumno.');
		}

	}

}
