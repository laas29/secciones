<?php
namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class ReportsController {

	private $c, $reportsModel, $respFormatter;

	public function __construct($container) {

		$this->c = $container;
		$this->reportsModel = new \App\Model\ReportsModel($container);
		$this->respFormatter = $container->get('responseFormatter');
	}

	public function getRequestedPlacesByProgram(Request $request, Response $response, $args) {

		$data = $request->getParsedBody();

		$reporte['campus'] = $this->reportsModel->getPreregistrationPerPrograms($data);

		$reporte['movilidad'] = $this->reportsModel->getPreregistrationMobilityPerPrograms($data);

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);

	}

	public function getPreregisteredStudents(Request $request, Response $response, $args) {

		$data = $request->getParsedBody();

		$reporte = $this->reportsModel->getPreregisteredStudents($data);

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);

	}

	public function getPreregisteredStudentsBySubject(Request $request, Response $response, $args) {

		$args['carreras'] = explode('/', $request->getAttribute('carreras'));

		$reporte = $this->reportsModel->getStudentsByCicleSubjectAndProgram($args);

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);

	}

	public function getPreregisteredMobilityStudentsBySubject(Request $request, Response $response, $args) {

		$args['carreras'] = explode('/', $request->getAttribute('carreras'));

		$reporte = $this->reportsModel->getStudentsMobilityByCicleSubjectAndProgram($args);

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);

	}

	/*Reportes para seccion de edicion*/

	public function getSearchParameters($params) {

		foreach ($params['filter'] as $key => $value) {
			if (!is_null($value)) {
				$row['fields'][$key] = $value;
			}
		}

		$page = intval($params['page']);
		$count = intval($params['count']);

		$row['skip'] = ($page - 1) * $count;
		$row['limit'] = $count;
		if (count($params['sorting'])) {
			$row['sortBy'] = key($params['sorting']);
			$row['sortOrder'] = current($params['sorting']);
		}

		return $row;

	}

	public function getAllStudents(Request $request, Response $response, $args) {

		$reporte = $this->reportsModel->getAllStudents($this->getSearchParameters($request->getParsedBody()));

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);

	}

	public function getAllPrograms(Request $request, Response $response, $args) {
		$reporte = $this->reportsModel->getAllPrograms($this->getSearchParameters($request->getParsedBody()));

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);

	}

	public function getProgramsBySubject(Request $request, Response $response, $args) {
		$reporte = $this->reportsModel->getProgramsBySubject($args);

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);
	}

	public function getAllCycles(Request $request, Response $response, $args) {
		$reporte = $this->reportsModel->getAllCycles($this->getSearchParameters($request->getParsedBody()));

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);
	}

	public function getAllCenters(Request $request, Response $response, $args) {
		$reporte = $this->reportsModel->getAllCenters($this->getSearchParameters($request->getParsedBody()));

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);
	}

	public function getAllDepartaments(Request $request, Response $response, $args) {
		$reporte = $this->reportsModel->getAllDepartaments($this->getSearchParameters($request->getParsedBody()));

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);
	}

	public function getAllSubjects(Request $request, Response $response, $args) {
		$reporte = $this->reportsModel->getAllSubjects($this->getSearchParameters($request->getParsedBody()));

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);
	}

	public function getAllDivisions(Request $request, Response $response, $args) {
		$reporte = $this->reportsModel->getAllDivisions($this->getSearchParameters($request->getParsedBody()));

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);
	}

	public function getAllUsers(Request $request, Response $response, $args) {
		$reporte = $this->reportsModel->getAllUsers($this->getSearchParameters($request->getParsedBody()));

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);
	}

	public function getAllPreregisterCycles(Request $request, Response $response, $args) {

		$reporte = $this->reportsModel->getAllPreregisterCycles($this->getSearchParameters($request->getParsedBody()));

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);

	}
}
