<?php
namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class PreregistrationController {

	/**
	 * @param $container
	 */
	public function __construct($container) {

		$this->container = $container;
		$this->preregistrationModel = new \App\Model\PreregistrationModel($container);
		$this->jwtSession = $this->container->jwtSession;
		$this->respFormatter = $container->get('responseFormatter');

	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param $args
	 * @return mixed
	 */
	public function getAllSubjects(Request $request, Response $response, $args) {

		$id_alumno = $args['id_alumno'];

		if ($this->container->jwtSession->role == 'alumno') {

			if ($id_alumno != $this->container->jwtSession->idAlumno) {

				return $this->respFormatter->getJsonResponse($response, 400, [], 'No tienes permisos para acceder a información de otro alumno.');

			}

		}

		$subjects = $this->preregistrationModel->getSubjectsOfStudent($id_alumno);

		$mobility = $this->preregistrationModel->getMobilityOfStudent($id_alumno);

		$allSubjects = [
			'materias' => $subjects,
			'movilidad' => $mobility,
		];

		return $this->respFormatter->getJsonResponse($response, 200, $allSubjects);

	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param $args
	 * @return mixed
	 */
	public function createPreregistration(Request $request, Response $response, $args) {

		$mobility = false;
		$preregistro = $request->getParsedBody();
		$row = array(
			'id_alumno' => $this->jwtSession->idAlumno,
			'id_ciclo' => $this->jwtSession->cicloActivo,
		);
		$subjects = [];

		$savedPre = $this->preregistrationModel->getPreregistration($row);

		if (count($savedPre)) {
			$preregistrationId = $savedPre[0]['id_preregistro'];
		} else {
			$this->preregistrationModel->createPreregistration($row);
			$preregistrationId = $this->preregistrationModel->getLastInsertedId();
		}

		/*
		echo "materias: ";
		print_r($preregistro['movilidad']);
		 */

		if (count($preregistro['materias'])) {

			foreach ($preregistro['materias'] as $key => $materia) {

				$subjects[] = [$preregistrationId, $materia['id_materia'], $materia['situacion'], $materia['horario']];

			}

			$this->preregistrationModel->updatePreregistrationSubjects(['id_preregistro' => $preregistrationId], $subjects);
			unset($subjects);
		} else {
			$this->preregistrationModel->deletePreregistrationSubjects(['id_preregistro' => $preregistrationId]);
		}

		if (count($preregistro['movilidad'])) {

			foreach ($preregistro['movilidad'] as $key => $movilidad) {
				$mobilitySubjects[] = [$movilidad['clave'], $movilidad['campus'], $preregistrationId];
			}

			$this->preregistrationModel->updatePreregistrationMobility(['id_preregistro' => $preregistrationId], $mobilitySubjects);
			unset($mobilitySubjects);

		} else {
			$this->preregistrationModel->deletePreregistrationMobility(['id_preregistro' => $preregistrationId]);
		}

		return $this->respFormatter->getJsonResponse($response, 201, []);

	}

}
