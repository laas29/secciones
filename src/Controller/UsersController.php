<?php
namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class UsersController {

	/**
	 * @param $container
	 */
	public function __construct($container) {

		$this->container = $container;
		$this->usersModel = new \App\Model\UsersModel($container);
		$this->respFormatter = $container->get('responseFormatter');

	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param $args
	 * @return mixed
	 */
	public function getUser(Request $request, Response $response, $args) {

		$email = $args['email'];

		if ($email != $this->container->jwtSession->email && $this->container->jwtSession->role == 'alumno') {

			return $this->respFormatter->getJsonResponse($response, 400, [], 'No tienes permisos para acceder a información de otro usuario.');

		}

		$results = $this->usersModel->getUserCredentials($email);

		if (count($results)) {
			unset($results[0]['contrasena']);
		}

		return $this->respFormatter->getJsonResponse($response, 200, $results);

	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param $args
	 * @return mixed
	 */
	public function checkCurrentUser(Request $request, Response $response, $args) {

		$student = $this->usersModel->studentExists($args["codigo"]);

		return $this->respFormatter->getJsonResponse($response, 200, $student);
	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param $args
	 */
	public function updateUser(Request $request, Response $response, $args) {

		$user = $request->getParsedBody();
		$row = array(
			':id_usuario' => $this->container->jwtSession->id_usuario,
			':nombre' => $user['nombre'],
			':apellidos' => $user['apellidos'],
			':puesto' => $user['puesto'],
			':email' => $user['email'],
		);

		if ($this->container->jwtSession->email != $user['email']) {

			$existingUser = $this->usersModel->userExists($user['email']);

			if (count($existingUser)) {
				return $this->respFormatter->getJsonResponse($response, 400, [], 'El email que ha definido lo tiene asignado otro usuario, si cree que esto es un error acuda con el administrador del sistema.');
			}

		}

		$this->usersModel->updateUser($row);

		$payload = [
			'id_usuario' => $this->container->jwtSession->id_usuario,
			'nombre' => $user['nombre'],
			'apellidos' => $user['apellidos'],
			'puesto' => $user['puesto'],
			'email' => $user['email'],
			'role' => $this->container->jwtSession->role,
		];

		$token = $this->container->jwtHelper->createToken($payload);

		$data = [
			'token' => $token,
		];

		return $this->respFormatter->getJsonResponse($response, 200, [$data]);

	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param $args
	 * @return mixed
	 */
	public function updateUserPassword(Request $request, Response $response, $args) {
		$user = $request->getParsedBody();
		$credentials = $this->usersModel->getUserCredentials($this->container->jwtSession->email)[0];

		if (password_verify($user['vieja_contrasena'], $credentials['contrasena'])) {

			$row = array(
				':id_usuario' => $this->container->jwtSession->id_usuario,
				':nueva_contrasena' => password_hash($user['nueva_contrasena'], PASSWORD_DEFAULT),
			);

			$this->usersModel->updatePassword($row);

		} else {
			return $this->respFormatter->getJsonResponse($response, 400, [], 'La la vieja contraseña no coincide con la proporcionada');
		}

		return $this->respFormatter->getJsonResponse($response, 200, []);

	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param $args
	 * @return mixed
	 */
	public function createUser(Request $request, Response $response, $args) {

		$user = $request->getParsedBody();

		$row = array(
			':nombre' => $user['nombre'],
			':apellidos' => $user['apellidos'],
			':puesto' => $user['puesto'],
			':email' => $user['email'],
			':contrasena' => password_hash($user['contrasena'], PASSWORD_DEFAULT),
			':role' => $credentials['role'],
		);

		$this->usersModel->createUser($row);
		$lastInsertedId = $this->usersModel->getLastInsertedId();

		if ($lastInsertedId > 0) {
			return $this->respFormatter->getJsonResponse($response, 201, ['id' => $lastInsertedId]);
		} else {
			return $this->respFormatter->getJsonResponse($response, 400, [], 'Hubo un error al crear al usuario.');
		}

	}

}
