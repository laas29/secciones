<?php
namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use \PHPMailer as PHPMailer;

final class MailerController {

	const ERROR_SENDING_EMAIL = 1;
	const SUCCESSFUL_EMAIL = 0;

	/**
	 * @param $container
	 */
	public function __construct($container) {

		$this->container = $container;
		$this->respFormatter = $container->get('responseFormatter');

	}

	/**
	 * @param $credentials
	 * @param $nueva_contrasena
	 */
	public function enviarCorreoCambioPswEstudiante($credentials, $nueva_contrasena) {

		$credentials = $credentials[0];

		$para = $credentials['email'];

		// título
		$titulo = 'Recuperacion de accesos para el sistema de pre-registro';

		// mensaje
		$mensaje = '
            <html>
                <head>
                  <title>Recuperacion de accesos para el sistema de pre-registro</title>
                </head>
                <body>
                  <p>
                    Tu usuario: ' . $credentials['codigo'] . '
                    <br>
                    Tu nueva contrasena para el sistema de pre-registro: ' . $nueva_contrasena . '
                  </p>
                  <p>
                    Recuerda que dentro de la aplicacion puedes modificar tus datos incluida la contrasena.
                  </p>
                </body>
            </html>
            ';


		if ($this->enviaEmail($titulo, $mensaje, $para, $credentials['nombre'])) {

			return self::SUCCESSFUL_EMAIL;

		} else {

			return self::ERROR_SENDING_EMAIL;

		}

	}

	public function enviaEmail($asunto, $contenidoHTML, $correoDestinatario, $nombreDestinatario){

		$enviado;
		// Instantiate Class  
		$mail = new PHPMailer();   
		$mail->CharSet = 'UTF-8';
		$mail->IsSMTP();                // Sets up a SMTP connection  
		$mail->SMTPAuth = true;         // Connection with the SMTP does require authorization    
		$mail->SMTPSecure = "ssl";      // Connect using a TLS connection  
		$mail->Host = "smtp.gmail.com";  //Gmail SMTP server address
		$mail->Port = 465;  //Gmail SMTP port
		$mail->Encoding = '7bit';
		 
		$mail->Username   = "preregistro.cucei@gmail.com"; // Your full Gmail address
		$mail->Password   = "preregistro.cucei.dcc16*"; // Your Gmail password

		$mail->SetFrom("preregistro.cucei@gmail.com","Preregistro CUCEI");
		$mail->AddReplyTo("preregistro.cucei@gmail.com","Preregistro CUCEI");
		$mail->Subject = $asunto; 
		$mail->MsgHTML($contenidoHTML);
		// Send To  
		$mail->AddAddress($correoDestinatario,$nombreDestinatario); // Where to send it - Recipient
		$enviado = $mail->Send();		// Send!  
		unset($mail);
		return $enviado;
		
	}

	/**
	 * @param $asunto
	 * @param $contenidoHTML
	 * @param $correoDestinatario
	 * @param $nombreDestinatario
	 * @return bool
	 */
	public function enviaEmail($asunto, $contenidoHTML, $correoDestinatario, $nombreDestinatario) {

		$enviado = false;
		// Instantiate Class
		$mail = new PHPMailer();
		$mail->CharSet = 'UTF-8';
		$mail->IsSMTP(); // Sets up a SMTP connection
		$mail->SMTPAuth = true; // Connection with the SMTP does require authorization
		$mail->SMTPSecure = "ssl"; // Connect using a TLS connection
		$mail->Host = "smtp.gmail.com"; //Gmail SMTP server address
		$mail->Port = 465; //Gmail SMTP port
		$mail->Encoding = '7bit';

		$mail->Username = "preregistro.cucei@gmail.com"; // Your full Gmail address
		$mail->Password = "preregistro.cucei.dcc16*"; // Your Gmail password

		$mail->SetFrom("preregistro.cucei@gmail.com", "Preregistro CUCEI");
		$mail->AddReplyTo("preregistro.cucei@gmail.com", "Preregistro CUCEI");
		$mail->Subject = $asunto;
		$mail->MsgHTML($contenidoHTML);
		// Send To
		$mail->AddAddress($correoDestinatario, $nombreDestinatario); // Where to send it - Recipient
		$enviado = $mail->Send(); // Send!
		unset($mail);
		return $enviado;

	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param $args
	 * @return mixed
	 */
	public function recoverStudentPassword(Request $request, Response $response, $args) {

		$data = $request->getParsedBody();

		$studentsModel = new \App\Model\StudentsModel($this->container);

		$credentials = $studentsModel->getStudentCredentials($data['codigo']);

		$nueva_contrasena = base64_encode(mt_rand(1, 9999999));

		if (count($credentials)) {

			if ($this->enviarCorreoCambioPswEstudiante($credentials, $nueva_contrasena) == self::SUCCESSFUL_EMAIL) {

				$row = [
					'nueva_contrasena' => password_hash($nueva_contrasena, PASSWORD_DEFAULT),
					'id_alumno' => $credentials[0]['id_alumno'],
				];
				$studentsModel->updatePassword($row);

				return $this->respFormatter->getJsonResponse($response, 200, [], 'Se ha enviado un correo a la direccion del usuario.');
			} else {
				return $this->respFormatter->getJsonResponse($response, 500, [], 'No se pudo enviar un correo a la direccion del usuario, intentelo más tarde');
			}

		} else {
			return $this->respFormatter->getJsonResponse($response, 400, [], 'No se tiene registro de ese código de estudiante.');
		}

	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param $args
	 * @return mixed
	 */
	public function recoverUserPassword(Request $request, Response $response, $args) {

		$data = $request->getParsedBody();

		$usersModel = new \App\Model\UsersModel($this->container);

		$credentials = $usersModel->getUserCredentials($data['email']);

		$nueva_contrasena = base64_encode(mt_rand(1, 9999999));

		if (count($credentials)) {

			if ($this->enviarCorreoCambioPswUsuario($credentials, $nueva_contrasena) == self::SUCCESSFUL_EMAIL) {

				$row = [
					'nueva_contrasena' => password_hash($nueva_contrasena, PASSWORD_DEFAULT),
					'id_usuario' => $credentials[0]['id_usuario'],
				];
				$usersModel->updatePassword($row);

				return $this->respFormatter->getJsonResponse($response, 200, [], 'Se ha enviado un correo a la direccion del usuario.');
			} else {
				return $this->respFormatter->getJsonResponse($response, 500, [], 'No se pudo enviar un correo a la direccion del usuario, intentelo más tarde');
			}

		} else {
			return $this->respFormatter->getJsonResponse($response, 400, [], 'No se tiene registro de ese usuario.');
		}

	}

	/**
	 * @param $credentials
	 * @param $nueva_contrasena
	 */
	public function enviarCorreoCambioPswUsuario($credentials, $nueva_contrasena) {

		$row = $credentials[0];

		$para = $row['email'];

		// título
		$titulo = 'Recuperacion de accesos para el sistema de pre-registro';

		// mensaje
		$mensaje = '
        <html>
            <head>
              <title>Recuperacion de accesos para el sistema de pre-registro</title>
            </head>
            <body>
              <p>
                Tu usuario: ' . $para . '
                <br>
                Tu nueva contrasena para el sistema de pre-registro: ' . $nueva_contrasena . '
              </p>
              <p>
                Recuerda que dentro de la aplicacion puedes modificar tus datos incluida la contrasena.
              </p>
            </body>
        </html>
        ';

		// Enviarlo
		if ($this->enviaEmail($titulo, $mensaje, $para, $row['nombre'])) {

			return self::SUCCESSFUL_EMAIL;

		} else {
			return self::ERROR_SENDING_EMAIL;
		}

	}

}
