<?php
namespace App\Controller;

use App\Model\CampusModel;
use App\Model\CyclesModel;
use App\Model\DepartamentsModel;
use App\Model\DivisionsModel;
use App\Model\ProgramsModel;
use App\Model\StudentsModel;
use App\Model\SubjectsModel;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class PublicListsController {

	/**
	 * @param $container
	 */
	public function __construct($container) {

		$this->container = $container;
		$this->jwt_settings = $this->container->get('settings')['jwt'];
		$this->respFormatter = $container->get('responseFormatter');

	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @return mixed
	 */
	public function getAllDepartaments(Request $request, Response $response) {

		$departamentsModel = new DepartamentsModel($this->container);

		$data = $departamentsModel->getAllDepartaments();

		return $this->respFormatter->getJsonResponse($response, 200, $data);

	}

	/**
	 * @param Request $request
	 * @param Response $response
	 */
	public function getAllCampus(Request $request, Response $response) {

		$campusModel = new CampusModel($this->container);

		$data = $campusModel->getAllPrograms($this->container);

		return $this->respFormatter->getJsonResponse($response, 200, $data);
	}

	/**
	 * @param Request $request
	 * @param Response $response
	 */
	public function getAllPrograms(Request $request, Response $response) {

		$programsModel = new ProgramsModel($this->container);

		$data = $programsModel->getAllPrograms();

		return $this->respFormatter->getJsonResponse($response, 200, $data);
	}

	/**
	 * @param Request $request
	 * @param Response $response
	 */
	public function getAllCycles(Request $request, Response $response) {

		$cyclesModel = new CyclesModel($this->container);

		$data = $cyclesModel->getAllCycles();

		return $this->respFormatter->getJsonResponse($response, 200, $data);
	}

	public function getAllDivisions(Request $request, Response $response) {

		$divisionsModel = new DivisionsModel($this->container);

		$data = $divisionsModel->getAllDivisions();

		return $this->respFormatter->getJsonResponse($response, 200, $data);
	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param $args
	 * @return mixed
	 */
	public function checkCurrentStudent(Request $request, Response $response, $args) {

		$studentsModel = new StudentsModel($this->container);

		$student = $studentsModel->studentExists($args["codigo"]);

		return $this->respFormatter->getJsonResponse($response, 200, $student);
	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param $args
	 * @return mixed
	 */
	public function checkCurrentProgram(Request $request, Response $response, $args) {

		$programsModel = new ProgramsModel($this->container);

		$program = $programsModel->programExists($args["clave"]);

		return $this->respFormatter->getJsonResponse($response, 200, $program);
	}

	public function checkSubjectExistence(Request $request, Response $response, $args) {

		$subjectsModel = new SubjectsModel($this->container);

		$subject = $subjectsModel->subjectExists($args['clave']);

		return $this->respFormatter->getJsonResponse($response, 200, $subject);

	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param $args
	 * @return mixed
	 */
	public function getAllProgramsByDepartament(Request $request, Response $response, $args) {

		$programsModel = new ProgramsModel($this->container);

		$programs = $programsModel->getProgramsByDepartament($args['id_departamento']);

		return $this->respFormatter->getJsonResponse($response, 200, $programs);

	}

}
