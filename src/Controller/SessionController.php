<?php
namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class SessionController
{

    /**
     * @var mixed
     */
    private $c, $db, $jwt_settings, $studentsModel, $respFormatter;

    /**
     * @param $container
     */
    public function __construct($container)
    {
        $this->c = $container;
        $this->respFormatter = $container->get('responseFormatter');
        $this->jwtHelper = $container->get('jwtHelper');
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $id
     * @return mixed
     */
    public function studentLogin(Request $request, Response $response, $id)
    {

        $studentsModel = new \App\Model\StudentsModel($this->c);
        $cyclesModel = new \App\Model\CyclesModel($this->c);
        $programsModel = new \App\Model\ProgramsModel($this->c);
        $loginData = $request->getParsedBody();
        $credentials = $studentsModel->getStudentCredentials($loginData['codigo']);
        $isActiveProgram = false;

        if (count($credentials)) {
            $credentials = $credentials[0];
        } else {
            return $this->respFormatter->getJsonResponse($response, 400, [], 'El usuario o la contraseña son incorrectos');
        }

        $preregistrationActiveCicle = $cyclesModel->getActiveCycleForPreregistration();
        $preregistrationActivePrograms = $programsModel->getActiveProgramsForPreregistration();

        if (count($preregistrationActiveCicle)) {

            $preregistrationActiveCicle = $preregistrationActiveCicle[0];

            foreach ($preregistrationActivePrograms as $key => $program) {

                if ($program['id_programa'] == $credentials['id_programa']) {
                    $isActiveProgram = true;
                    break;
                }

            }

            if (!$isActiveProgram) {
                return $this->respFormatter->getJsonResponse($response, 400, [], 'No está activo el pre-registro para tu carrera.');
            }

        } else {
            return $this->respFormatter->getJsonResponse($response, 400, [], 'No está activo el pre-registro.');
        }

        if (password_verify($loginData['contrasena'], $credentials['contrasena'])) {

            $payload = [
                'role' => $credentials['role'],
                'email' => $credentials['email'],
                'carrera' => $credentials['id_programa'],
                'nombre' => $credentials['nombre'],
                'apellidos' => $credentials['apellidos'],
                'codigo' => $credentials['codigo'],
                'idAlumno' => $credentials['id_alumno'],
                'cicloActivo' => $preregistrationActiveCicle['id_ciclo'],
            ];
            $token = $this->jwtHelper->createToken($payload);
            $data = [
                'token' => $token,
            ];

            return $this->respFormatter->getJsonResponse($response, 201, [$data]);

        } else {

            return $this->respFormatter->getJsonResponse($response, 400, [], 'El usuario o la contraseña son incorrectos');

        }

    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $id
     * @return mixed
     */
    public function userLogin(Request $request, Response $response, $id)
    {

        // echo 'pruebamesta';

        $usersModel = new \App\Model\UsersModel($this->c);

        $loginData = $request->getParsedBody();
        $credentials = $usersModel->getUserCredentials($loginData['email']);

        if (count($credentials)) {
            $credentials = $credentials[0];
        } else {
            return $this->respFormatter->getJsonResponse($response, 400, [], 'El usuario o la contraseña son incorrectos');
        }

        if (password_verify($loginData['contrasena'], $credentials['contrasena'])) {

            $payload = [
                'id_usuario' => $credentials['id_usuario'],
                'email' => $credentials['email'],
                'nombre' => $credentials['nombre'],
                'apellidos' => $credentials['apellidos'],
                'puesto' => $credentials['puesto'],
                'role' => $credentials['role'],
            ];

            $token = $this->jwtHelper->createToken($payload);
            $data = [
                'token' => $token,
            ];

            return $this->respFormatter->getJsonResponse($response, 201, [$data]);

        } else {

            return $this->respFormatter->getJsonResponse($response, 400, [], 'El usuario o la contraseña son incorrectos');

        }

    }

}

/*
id_usuario
usuario
password
role
 */
