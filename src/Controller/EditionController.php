<?php
namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class EditionController {

	private $c, $reportsModel, $respFormatter;

	public function __construct($container) {

		$this->c = $container;
		$this->editionModel = new \App\Model\EditionModel($container);
		$this->respFormatter = $container->get('responseFormatter');
	}

	public function getStudent(Request $request, Response $response, $args) {

		$reporte = $this->editionModel->getStudent($args);

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);

	}

	public function saveStudent(Request $request, Response $response, $args) {

		$mailerController = new \App\Controller\MailerController($this->c);
		$studentModel = new \App\Model\StudentsModel($this->c);

		$params = $request->getParsedBody();

		if (isset($params['id_alumno'])) {
			$reporte = $this->editionModel->updateStudent($params);
		} else {

			if (count($studentModel->studentExists($params['codigo']))) {
				return $this->respFormatter->getJsonResponse($response, 500, [], 'El código proporcionado ya está registrado, por favor revise el código de alumno que desea asignar.');
			}

			$contrasena = base64_encode(mt_rand(1, 99999999));

			if ($mailerController->enviarCorreoCambioPswEstudiante([$params], $contrasena) == $mailerController::SUCCESSFUL_EMAIL) {

				$params['contrasena'] = password_hash($contrasena, PASSWORD_DEFAULT);
				$params['id_alumno'] = $this->editionModel->createStudent($params);
				return $this->respFormatter->getJsonResponse($response, 200, [], 'Se ha enviado un correo a la direccion del usuario.');

			} else {

				return $this->respFormatter->getJsonResponse($response, 500, [], 'No se pudo enviar un correo a la direccion del usuario, intentelo más tarde');

			}
		}

		return $this->respFormatter->getJsonResponse($response, 200, []);

	}

	public function removeStudent(Request $request, Response $response, $args) {

		$this->editionModel->deleteStudent($args);

		return $this->respFormatter->getJsonResponse($response, 200, []);

	}

	public function getProgram(Request $request, Response $response, $args) {

		$reporte = $this->editionModel->getProgram($args);

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);

	}

	public function saveProgram(Request $request, Response $response, $args) {

		$programsModel = new \App\Model\ProgramsModel($this->c);

		$params = $request->getParsedBody();

		if (isset($params['id_programa'])) {

			$this->editionModel->updateProgramSubjects($params);

			$this->editionModel->updateProgram($params);

		} else {

			if (count($programsModel->programExists($params['clave']))) {
				return $this->respFormatter->getJsonResponse($response, 500, [], 'La clave proporcionada ya está registrada, por favor revise la clave de programa/carrera que desea asignar.');
			}

			$formated_params = [
				'clave' => $params['clave'],
				'nombre' => $params['nombre'],
				'id_departamento' => $params['id_departamento'],
				'preregistro_activo' => $params['preregistro_activo'],
			];

			$id_programa = $this->editionModel->createProgram($formated_params);

			if ($id_programa != 0) {
				$params['id_programa'] = $id_programa;
				$this->editionModel->updateProgramSubjects($params);
			}

		}

		return $this->respFormatter->getJsonResponse($response, 200, []);

	}

	public function removeProgram(Request $request, Response $response, $args) {

		if ($this->editionModel->deleteProgram($args) == 1) {
			return $this->respFormatter->getJsonResponse($response, 500, [], 'No se eliminará el programa porque hay alumnos registrados al programa.');
		} else {
			return $this->respFormatter->getJsonResponse($response, 200, []);
		}
	}

	public function getCycle(Request $request, Response $response, $args) {
		$reporte = $this->editionModel->getCycle($args);

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);
	}

	public function saveCycle(Request $request, Response $response, $args) {
		$params = $request->getParsedBody();

		// deshabilitamos todos los ciclos activos para preregistro
		if ($params['preregistro_activo'] == '1') {
			$this->editionModel->disableAllPreRegisterCycles();
		}

		if (isset($params['id_ciclo'])) {
			$this->editionModel->updateCycle($params);
		} else {
			$this->editionModel->createCycle($params);
		}

		return $this->respFormatter->getJsonResponse($response, 200, []);
	}

	public function removeCycle(Request $request, Response $response, $args) {

		switch ($this->editionModel->deleteCycle($args)) {
		case 1:return $this->respFormatter->getJsonResponse($response, 500, [], 'El ciclo no puede ser borrado mientras haya alumnos con ese ciclo registrado como generación');
		case 2:return $this->respFormatter->getJsonResponse($response, 500, [], 'El ciclo no puede ser borrado mientras haya preregistros de ese ciclo');
		case 0:return $this->respFormatter->getJsonResponse($response, 200, []);
		}

	}

	public function getCenter(Request $request, Response $response, $args) {
		$reporte = $this->editionModel->getCenter($args);

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);
	}

	public function saveCenter(Request $request, Response $response, $args) {

		$params = $request->getParsedBody();

		if (isset($params['id_centro'])) {
			$this->editionModel->updateCenter($params);
		} else {
			$this->editionModel->createCenter($params);
		}

		return $this->respFormatter->getJsonResponse($response, 200, []);

	}

	public function removeCenter(Request $request, Response $response, $args) {

		$this->editionModel->deleteCenter($args);
		return $this->respFormatter->getJsonResponse($response, 200, []);

	}

	public function getDepartament(Request $request, Response $response, $args) {
		$reporte = $this->editionModel->getDepartament($args);

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);
	}

	public function saveDepartament(Request $request, Response $response, $args) {

		$params = $request->getParsedBody();

		if (isset($params['id_departamento'])) {
			$this->editionModel->updateDepartament($params);
		} else {
			$this->editionModel->createDepartament($params);
		}

		return $this->respFormatter->getJsonResponse($response, 200, []);

	}

	public function getSubject(Request $request, Response $response, $args) {
		$reporte = $this->editionModel->getSubject($args);

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);
	}

	public function saveSubject(Request $request, Response $response, $args) {

		$subjectsModel = new \App\Model\SubjectsModel($this->c);

		$ids = [];
		$estaRelacionadaConCarrera = false;
		$params = $request->getParsedBody();

		if (isset($params['idsElegidos'])) {
			$ids['idsElegidos'] = $params['idsElegidos'];
			unset($params['idsElegidos']);
			$estaRelacionadaConCarrera = true;
		}
		if (isset($params['idsRemovidos'])) {
			$ids['idsRemovidos'] = $params['idsRemovidos'];
			unset($params['idsRemovidos']);
			$estaRelacionadaConCarrera = true;
		}

		if (isset($params['id_materia'])) {

			$this->editionModel->updateSubject($params);

			$ids['id_materia'] = $params['id_materia'];

			if ($estaRelacionadaConCarrera) {
				$this->editionModel->updateSubjectPrograms($ids);
			}

		} else {

			if (count($subjectsModel->subjectExists($params['clave']))) {
				return $this->respFormatter->getJsonResponse($response, 500, [], 'La clave de esa materia ya está registrada por otra materia.');
			}

			$ids['id_materia'] = $this->editionModel->createSubject($params);

			if ($estaRelacionadaConCarrera) {
				$this->editionModel->updateSubjectPrograms($ids);
			}

		}

		return $this->respFormatter->getJsonResponse($response, 200, []);
	}

	public function removeSubject(Request $request, Response $response, $args) {
		if ($this->editionModel->deleteSubject($args) == 1) {
			return $this->respFormatter->getJsonResponse($response, 500, [], 'No se eliminará la materia porque esta siendo usada en preregistros.');
		} else {
			return $this->respFormatter->getJsonResponse($response, 200, []);
		}
	}

	public function getUser(Request $request, Response $response, $args) {
		$reporte = $this->editionModel->getUser($args);

		return $this->respFormatter->getJsonResponse($response, 200, $reporte);
	}

	public function saveUser(Request $request, Response $response, $args) {
		$mailerController = new \App\Controller\MailerController($this->c);
		$userModel = new \App\Model\UsersModel($this->c);

		/*
		Hacer lo mismo que en saveStudent
		 */

		$params = $request->getParsedBody();

		if (isset($params['id_usuario'])) {
			$this->editionModel->updateUser($params);
		} else {

			if (count($userModel->userExists($params['email']))) {

				return $this->respFormatter->getJsonResponse($response, 500, [], 'El email proporcionado ya está registrado.');

			} else {

				$contrasena = base64_encode(mt_rand(1, 99999999));

				if ($mailerController->enviarCorreoCambioPswUsuario([$params], $contrasena) == $mailerController::SUCCESSFUL_EMAIL) {

					$params['contrasena'] = password_hash($contrasena, PASSWORD_DEFAULT);
					$params['id_usuario'] = $this->editionModel->createUser($params);
					return $this->respFormatter->getJsonResponse($response, 200, [], 'Se ha enviado un correo a la direccion del usuario.');

				} else {

					return $this->respFormatter->getJsonResponse($response, 500, [], 'No se pudo enviar un correo a la direccion del usuario, intentelo más tarde');

				}

			}

		}

		return $this->respFormatter->getJsonResponse($response, 200, []);
	}

	public function removeUser(Request $request, Response $response, $args) {
		$this->editionModel->deleteUser($args);
		return $this->respFormatter->getJsonResponse($response, 200, []);
	}

	public function removePreregistration(Request $request, Response $response, $args) {
		if ($this->editionModel->deletePreregistration($args) == 1) {
			return $this->respFormatter->getJsonResponse($response, 500, [], 'No se puede eliminar el preregistro si esta activo.');
		} else {
			return $this->respFormatter->getJsonResponse($response, 200, []);
		}
	}

}
