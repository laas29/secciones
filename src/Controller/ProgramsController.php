<?php
namespace App\Controller;

use App\Model\ProgramsModel;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class ProgramsController
{

    private $c, $programsModel, $respFormatter;

    public function __construct($container)
    {

        $this->c = $container;
        $this->programsModel = new ProgramsModel($container);
        $this->respFormatter = $container->get('responseFormatter');
    }

    public function getSubjects(Request $resquest, Response $response, $args)
    {

        $materias = $this->programsModel->getSubjects($args['id_programa']);
        return $this->respFormatter->getJsonResponse($response, 200, $materias);

    }

}
