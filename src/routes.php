<?php

use App\Middleware\ValidationMiddleware as V;

$c = $app->getContainer();

$app->get('/', function ($request, $response, $args) {
// Sample log message
	//$this->logger->info('Slim-Skeleton '/' route');

// Render index view
	//return $this->renderer->render($response, 'index.phtml', $args);
});

$app->group('/scraper', function () {

	$this->get('/materias-x-programa', 'App\Controller\ScraperController:executeCURL')
		->setName('scraper');

});

$app->group('/programa', function () use ($c) {

	$this->get('/{id_programa:\d+}/materias', 'App\Controller\ProgramsController:getSubjects')
		->setName('student.getProgramSubjects')
		->add(new V(
			$c,
			[
				['id_programa', ['intVal'], [1, 11], 'Carrera'],
			]
		));

});

$app->group('/preregistro', function () use ($c) {

	$this->post('', 'App\Controller\PreregistrationController:createPreregistration')
		->setName('student.createPreregistration')
		->add(new V(
			$c,
			[
				[
					'materias',
					[
						['id_materia', ['intVal'], [1, 11], 'id_materia property inside materias arrays'],
						['situacion', ['alnum'], [6, 11], 'situacion property inside materias arrays'],
						['horario', ['alpha'], [8, 10], 'horario property inside materias arrays'],
					],
					null,
					'materias array',
					['each'],
				],
				[
					'movilidad',
					[
						['clave', ['alnum'], [4, 10], 'clave property inside movilidad arrays'],
						['campus', ['intVal'], [1, 11], 'campus property inside movilidad arrays'],
					],
					null,
					'movilidad array',
					['each', 'optional'],
				],
			]
		));

	$this->get('/materias/alumno/{id_alumno:\d+}', 'App\Controller\PreregistrationController:getAllSubjects')
		->setName('student.preregistration.getAllSubjects')
		->add(new V(
			$c,
			[
				['id_alumno', ['intVal'], [1, 11], 'id de alumno'],
			]
		));

});

$app->group('/reportes', function () use ($c) {

	$this->post('/por-carreras', 'App\Controller\ReportsController:getRequestedPlacesByProgram')
		->setName('generate.reports.byProgram')
		->add(new V(
			$c,
			[
				['carreras', ['intVal'], [1, 11], 'ids de carreras', ['each']],
				['id_ciclo', ['intVal'], [1, 11], 'id ciclo'],
			]
		));

	$this->post('/alumnos-solicitantes', 'App\Controller\ReportsController:getPreregisteredStudents')
		->setName('generate.reports.preregistredStudent')
		->add(new V(
			$c,
			[
				['carreras', ['intVal'], [1, 11], 'ids de carreras', ['each']],
				['id_ciclo', ['intVal'], [1, 11], 'id ciclo'],
			]
		));

	$this->get('/ciclo/{id_ciclo:\d+}/materia/{clave}/programas/{carreras:.*}', 'App\Controller\ReportsController:getPreregisteredStudentsBySubject')
		->setName('generate.reports.preregistredStudent.BySubject')
		->add(new V(
			$c,
			[
				['clave', ['alnum'], [1, 10], 'clave materia'],
				['id_ciclo', ['intVal'], [1, 11], 'ciclo'],
				['carreras', ['stringType'], [1, 500], 'carreras'],
			]
		));

	$this->get('/movilidad/ciclo/{id_ciclo:\d+}/materia/{clave}/centro/{id_centro:\d+}/programas/{carreras:.*}', 'App\Controller\ReportsController:getPreregisteredMobilityStudentsBySubject')
		->setName('generate.mobility.reports.preregistredStudent.BySubject')
		->add(new V(
			$c,
			[
				['clave', ['alnum'], [1, 10], 'clave materia'],
				['id_ciclo', ['intVal'], [1, 11], 'ciclo'],
				['id_centro', ['intVal'], [1, 11], 'id centro'],
				['carreras', ['stringType', 'notEmpty'], [1, 500], 'carreras'],
			]
		));

	// reportes para la edicion

	/*
	{"page":1,"count":10,"filter":{"apellidos":"asd","email":"a","codigo":"2","nombre":"a","generacion":"a","programa":"a"},"sorting":{"cupos":"desc"},"group":{}}
	 */

	$this->post('/alumnos', 'App\Controller\ReportsController:getAllStudents')
		->setName('generate.reports.allStudents')
		->add(new V(
			$c,
			[
				['page', ['intVal'], [1, 11], 'page', ['optional']],
				['count', ['intVal'], [1, 11], 'count', ['optional']],
				[
					'filter',
					[
						['apellidos', ['stringType'], [null, 100], 'apellidos property inside filter array', ['optional']],
						['email', ['stringType'], [null, 100], 'email property inside filter array', ['optional']],
						['codigo', ['stringType'], [null, 20], 'codigo property inside filter array', ['optional']],
						['nombre', ['stringType'], [null, 100], 'nombre property inside filter array', ['optional']],
						['generacion', ['stringType'], [null, 60], 'generacion property inside filter array', ['optional']],
						['programa', ['stringType'], [null, 50], 'programa property inside filter array', ['optional']],
					],
					null,
					'filter array',
					[],
				],
				[
					'sorting',
					[
						['apellidos', ['alpha'], [3, 4], 'apellidos property inside sorting array', ['optional']],
						['email', ['alpha'], [3, 4], 'email property inside sorting array', ['optional']],
						['codigo', ['alpha'], [3, 4], 'codigo property inside sorting array', ['optional']],
						['nombre', ['alpha'], [3, 4], 'nombre property inside sorting array', ['optional']],
						['generacion', ['alpha'], [3, 4], 'generacion property inside sorting array', ['optional']],
						['programa', ['alpha'], [3, 4], 'programa property inside sorting array', ['optional']],
					],
					null,
					'sorting array',
					[],
				],
			]
		)
		);

	$this->post('/programas', 'App\Controller\ReportsController:getAllPrograms')
		->setName('generate.reports.allPrograms')
		->add(new V(
			$c,
			[
				['page', ['intVal'], [1, 11], 'page', ['optional']],
				['count', ['intVal'], [1, 11], 'count', ['optional']],
				[
					'filter',
					[
						['clave', ['stringType'], [null, 100], 'clave property inside filter array', ['optional']],
						['nombre', ['stringType'], [null, 100], 'nombre property inside filter array', ['optional']],
						['departamento', ['stringType'], [null, 100], 'departamento property inside filter array', ['optional']],
						['preregistro_activo', ['stringType'], [null, 1], 'preregistro_activo property inside filter array', ['optional']],
					],
					null,
					'filter array',
					[],
				],
				[
					'sorting',
					[
						['clave', ['alpha'], [3, 4], 'apellidos property inside sorting array', ['optional']],
						['nombre', ['alpha'], [3, 4], 'email property inside sorting array', ['optional']],
						['departamento', ['alpha'], [3, 4], 'codigo property inside sorting array', ['optional']],
						['preregistro_activo', ['alpha'], [3, 4], 'nombre property inside sorting array', ['optional']],
					],
					null,
					'sorting array',
					[],
				],
			]
		)
		);

	$this->post('/centros', 'App\Controller\ReportsController:getAllCenters')
		->setName('generate.reports.allPrograms')
		->add(new V(
			$c,
			[
				['page', ['intVal'], [1, 11], 'page', ['optional']],
				['count', ['intVal'], [1, 11], 'count', ['optional']],
				[
					'filter',
					[
						['clave', ['stringType'], [null, 100], 'clave property inside filter array', ['optional']],
						['nombre', ['stringType'], [null, 100], 'nombre property inside filter array', ['optional']],
					],
					null,
					'filter array',
					[],
				],
				[
					'sorting',
					[
						['clave', ['alpha'], [3, 4], 'clave property inside sorting array', ['optional']],
						['nombre', ['alpha'], [3, 4], 'nombre property inside sorting array', ['optional']],
					],
					null,
					'sorting array',
					[],
				],
			]
		)
		);

	$this->post('/materias', 'App\Controller\ReportsController:getAllSubjects')
		->setName('generate.reports.allSubjects')
		->add(new V(
			$c,
			[
				['page', ['intVal'], [1, 11], 'page', ['optional']],
				['count', ['intVal'], [1, 11], 'count', ['optional']],
				[
					'filter',
					[
						['clave', ['stringType'], [null, 10], 'clave property inside filter array', ['optional']],
						['nombre', ['stringType'], [null, 150], 'nombre property inside filter array', ['optional']],
						['subj', ['stringType'], [null, 10], 'subj property inside filter array', ['optional']],
						['creditos', ['stringType'], [null, 100], 'creditos property inside filter array', ['optional']],
						['practica', ['stringType'], [null, 100], 'practica property inside filter array', ['optional']],
						['teoria', ['stringType'], [null, 100], 'teoria property inside filter array', ['optional']],
						['tipo', ['stringType'], [null, 100], 'tipo property inside filter array', ['optional']],
						['nivel', ['stringType'], [null, 100], 'nivel property inside filter array', ['optional']],
						['carrera', ['stringType'], [null, 100], 'carrera property inside filter array', ['optional']],
						['id_programa', ['intVal'], [1, 11], 'id_programa property inside filter array', ['optional']],
						['preregistro_activo', ['stringType'], [null, 1], 'preregistro_activo property inside filter array', ['optional']],
					],
					null,
					'filter array',
					[],
				],
				[
					'sorting',
					[
						['clave', ['alpha'], [3, 4], 'clave property inside sorting array', ['optional']],
						['nombre', ['alpha'], [3, 4], 'nombre property inside sorting array', ['optional']],
						['subj', ['stringType'], [3, 4], 'subj property inside sorting array', ['optional']],
						['creditos', ['alpha'], [3, 4], 'creditos property inside sorting array', ['optional']],
						['practica', ['alpha'], [3, 4], 'practica property inside sorting array', ['optional']],
						['teoria', ['alpha'], [3, 4], 'teoria property inside sorting array', ['optional']],
						['tipo', ['alpha'], [3, 4], 'tipo property inside sorting array', ['optional']],
						['nivel', ['alpha'], [3, 4], 'nivel property inside sorting array', ['optional']],
						['carrera', ['alpha'], [3, 4], 'carrera property inside sorting array', ['optional']],
						['preregistro_activo', ['alpha'], [3, 4], 'preregistro_activo property inside sorting array', ['optional']],

					],
					null,
					'sorting array',
					[],
				],

			]
		)
		);

	$this->get('/programas-x-materia/{id_materia:\d+}', 'App\Controller\ReportsController:getProgramsBySubject')->setName('generate.reports.programsBySubject');

	$this->post('/ciclos', 'App\Controller\ReportsController:getAllCycles')
		->setName('generate.reports.allCycles')
		->add(new V(
			$c,
			[
				['page', ['intVal'], [1, 11], 'page', ['optional']],
				['count', ['intVal'], [1, 11], 'count', ['optional']],
				[
					'filter',
					[
						['clave', ['stringType'], [null, 100], 'clave property inside filter array', ['optional']],
						['nombre', ['stringType'], [null, 100], 'nombre property inside filter array', ['optional']],
						['preregistro', ['stringType'], [null, 1], 'preregistro_activo property inside filter array', ['optional']],
					],
					null,
					'filter array',
					[],
				],
				[
					'sorting',
					[
						['clave', ['alpha'], [3, 4], 'clave property inside sorting array', ['optional']],
						['nombre', ['alpha'], [3, 4], 'nombre property inside sorting array', ['optional']],
						['preregistro', ['alpha'], [3, 4], 'preregistro property inside sorting array', ['optional']],
					],
					null,
					'sorting array',
					[],
				],
			]
		)
		);

	$this->post('/departamentos', 'App\Controller\ReportsController:getAllDepartaments')
		->setName('generate.reports.allDepartments')
		->add(new V(
			$c,
			[
				['page', ['intVal'], [1, 11], 'page', ['optional']],
				['count', ['intVal'], [1, 11], 'count', ['optional']],
				[
					'filter',
					[
						['nombre', ['stringType'], [null, 100], 'clave property inside filter array', ['optional']],
						['clave', ['stringType'], [null, 100], 'nombre property inside filter array', ['optional']],
						['division', ['stringType'], [null, 100], 'departamento property inside filter array', ['optional']],
					],
					null,
					'filter array',
					[],
				],
				[
					'sorting',
					[
						['clave', ['alpha'], [3, 4], 'clave property inside sorting array', ['optional']],
						['nombre', ['alpha'], [3, 4], 'nombre property inside sorting array', ['optional']],
						['division', ['alpha'], [3, 4], 'division property inside sorting array', ['optional']],

					],
					null,
					'sorting array',
					[],
				],
			]
		)
		);

	$this->post('/usuarios', 'App\Controller\ReportsController:getAllUsers')
		->setName('generate.reports.allUsers')
		->add(new V(
			$c,
			[
				['page', ['intVal'], [1, 11], 'page', ['optional']],
				['count', ['intVal'], [1, 11], 'count', ['optional']],
				[
					'filter',
					[
						['email', ['stringType'], [null, 100], 'email property inside filter array', ['optional']],
						['nombre', ['stringType'], [null, 100], 'nombre property inside filter array', ['optional']],
						['apellidos', ['stringType'], [null, 100], 'apellidos property inside filter array', ['optional']],
						['puesto', ['stringType'], [null, 100], 'puesto property inside filter array', ['optional']],
						['role', ['stringType'], [null, 5], 'role property inside filter array', ['optional']],
					],
					null,
					'filter array',
					[],
				],
				[
					'sorting',
					[
						['email', ['alpha'], [3, 4], 'email property inside sorting array', ['optional']],
						['nombre', ['alpha'], [3, 4], 'nombre property inside sorting array', ['optional']],
						['apellidos', ['alpha'], [3, 4], 'apellidos property inside sorting array', ['optional']],
						['puesto', ['alpha'], [3, 4], 'puesto property inside sorting array', ['optional']],
						['role', ['alpha'], [3, 4], 'role property inside sorting array', ['optional']],
					],
					null,
					'sorting array',
					[],
				],
			]
		)
		);

	$this->post('/preregistros', 'App\Controller\ReportsController:getAllPreregisterCycles')
		->setName('generate.reports.allPreregisterCycles')
		->add(new V(
			$c,
			[
				['page', ['intVal'], [1, 11], 'page', ['optional']],
				['count', ['intVal'], [1, 11], 'count', ['optional']],
				[
					'filter',
					[
						['nombre', ['stringType'], [null, 60], 'nombre property inside filter array', ['optional']],
						['preregistro_activo', ['stringType'], [null, 1], 'preregistro_activo property inside filter array', ['optional']],
					],
					null,
					'filter array',
					[],
				],
				[
					'sorting',
					[
						['nombre', ['alpha'], [3, 4], 'nombre property inside sorting array', ['optional']],
						['preregistro_activo', ['alpha'], [3, 4], 'nombre property inside sorting array', ['optional']],
					],
					null,
					'sorting array',
					[],
				],
			]
		)
		);

});

$app->group('/recurso', function () use ($c) {

	// Due to the behaviour of browsers when sending PUT or DELETE request, you must add the OPTIONS method. Read about preflight.

	$this->group('/alumno[/{id_alumno:\d+}]', function () use ($c) {

		$this->map(['PUT', 'POST', 'OPTIONS'], '', 'App\Controller\EditionController:saveStudent')
			->setName('admin.save.student')
			->add(new V(
				$c,
				[
					['id_alumno', ['intVal'], [1, 11], 'ID Alumno', ['optional']],
					['nombre', ['stringType'], [null, 100], 'Nombre'],
					['apellidos', ['stringType'], [null, 100], 'Apellidos'],
					['codigo', ['alnum'], [null, 20], 'Código'],
					['email', ['email'], [null, 100], 'Correo Electronico'],
					['id_programa', ['intVal'], [null, 11], 'Carrera'],
					['id_generacion', ['intVal'], [null, null], 'Generación'],
				]
			));

		$this->delete('', 'App\Controller\EditionController:removeStudent')
			->setName('admin.remove.student')
			->add(new V(
				$c,
				[
					['id_alumno', ['intVal'], [1, 11], 'ID Alumno'],
				]
			));

		$this->get('', 'App\Controller\EditionController:getStudent')->setName('get.resource.student');
	});

	$this->group('/programa[/{id_programa:\d+}]', function () use ($c) {

		$this->map(['PUT', 'POST', 'OPTIONS'], '', 'App\Controller\EditionController:saveProgram')
			->setName('admin.save.program')
			->add(new V(
				$c,
				[
					['id_programa', ['intVal'], [1, 11], 'ID Programa', ['optional']],
					['nombre', ['stringType'], [1, 50], 'Nombre'],
					['clave', ['alnum'], [1, 10], 'Clave'],
					['id_departamento', ['intVal'], [1, 11], 'Código'],
					['preregistro_activo', ['stringType'], [1, 1], 'Preregistro activo'],
					['idsElegidos', ['intVal'], [1, 11], 'ids de materias añadidas', ['optional', 'each']],
					['idsRemovidos', ['intVal'], [1, 11], 'ids de materias removidas', ['optional', 'each']],
				]
			));

		$this->delete('', 'App\Controller\EditionController:removeProgram')
			->setName('admin.remove.program')
			->add(new V(
				$c,
				[
					['id_programa', ['intVal'], [1, 11], 'ID Programa'],
				]
			));

		$this->get('', 'App\Controller\EditionController:getProgram')->setName('get.program');

	});

	$this->group('/materia[/{id_materia:\d+}]', function () use ($c) {

		$this->map(['PUT', 'POST', 'OPTIONS'], '', 'App\Controller\EditionController:saveSubject')
			->setName('admin.save.subject')
			->add(new V(
				$c,
				[
					['id_materia', ['intVal'], [1, 11], 'ID Materia', ['optional']],
					['subj', ['alnum'], [1, 10], 'Subj'],
					['clave', ['alnum'], [1, 10], 'Clave'],
					['nombre', ['stringType'], [1, 150], 'Nombre'],
					['subj', ['stringType'], [1, 10], 'subj property inside filter array'],
					['creditos', ['intVal'], [1, 2], 'Creditos'],
					['carga_hor_teo', ['intVal'], [1, 3], 'Carga horaria teoría'],
					['carga_hor_pra', ['intVal'], [1, 3], 'Carga horaria práctica'],
					['tipo', ['alnum'], [1, 15], 'Clave'],
					['nivel', ['alnum'], [1, 15], 'Nivel'],
					['programas', ['intVal'], [1, 11], 'IDs de Programas', ['each', 'optional']],
					['preregistro_activo', ['stringType'], [1, 1], 'Preregistro activo'],
				]
			));

		$this->delete('', 'App\Controller\EditionController:removeSubject')
			->setName('admin.remove.subject')
			->add(new V(
				$c,
				[
					['id_materia', ['intVal'], [1, 11], 'ID Materia'],
				]
			));

		$this->get('', 'App\Controller\EditionController:getSubject')->setName('get.subject');

	});

	$this->group('/ciclo[/{id_ciclo:\d+}]', function () use ($c) {

		$this->map(['PUT', 'POST', 'OPTIONS'], '', 'App\Controller\EditionController:saveCycle')
			->setName('admin.save.cycle')
			->add(new V(
				$c,
				[
					['id_ciclo', ['intVal'], [1, 11], 'ID Ciclo', ['optional']],
					['nombre', ['stringType'], [1, 60], 'Nombre'],
					['clave', ['alnum'], [null, 10], 'Clave'],
					['preregistro_activo', ['stringType'], [1, 1], 'Preregistro activo'],
				]
			));

		$this->delete('', 'App\Controller\EditionController:removeCycle')
			->setName('admin.remove.cycle')
			->add(new V(
				$c,
				[
					['id_ciclo', ['intVal'], [1, 11], 'ID Ciclo'],
				]
			));

		$this->get('', 'App\Controller\EditionController:getCycle')->setName('get.cycle');

	});

	$this->group('/centro[/{id_centro:\d+}]', function () use ($c) {

		$this->map(['PUT', 'POST', 'OPTIONS'], '', 'App\Controller\EditionController:saveCenter')
			->setName('admin.save.center')
			->add(new V(
				$c,
				[
					['id_centro', ['intVal'], [1, 11], 'ID Centro', ['optional']],
					['nombre', ['stringType'], [1, 50], 'Nombre'],
					['clave', ['alnum'], [null, 10], 'Clave'],
				]
			));

		$this->delete('', 'App\Controller\EditionController:removeCenter')
			->setName('admin.remove.center')
			->add(new V(
				$c,
				[
					['id_centro', ['intVal'], [1, 11], 'ID Centro'],
				]
			));

		$this->get('', 'App\Controller\EditionController:getCenter')->setName('get.center');

	});

	$this->map(['DELETE', 'OPTIONS'], '/preregistro[/{id_ciclo:\d+}]', 'App\Controller\EditionController:removePreregistration')
		->setName('admin.remove.preregistration')
		->add(new V(
			$c,
			[
				['id_ciclo', ['intVal'], [1, 11], 'ID Ciclo'],
			]
		));

	$this->map(['PUT', 'POST', 'OPTIONS'], '/departamento[/{id_departamento:\d+}]', 'App\Controller\EditionController:saveDepartament')
		->setName('admin.save.departament')
		->add(new V(
			$c,
			[
				['id_departament', ['intVal'], [1, 11], 'ID Departamento', ['optional']],
				['nombre', ['stringType'], [1, 50], 'Nombre'],
				['clave', ['alnum'], [1, 10], 'Clave'],
				['id_division', ['intVal'], [1, 11], 'ID división'],
			]
		));

	$this->get('/departamento/{id_departamento:\d+}', 'App\Controller\EditionController:getDepartament')->setName('get.departament');

	$this->group('/usuario[/{id_usuario:\d+}]', function () use ($c) {

		$this->map(['PUT', 'POST', 'OPTIONS'], '', 'App\Controller\EditionController:saveUser')
			->setName('admin.save.user')
			->add(new V(
				$c,
				[
					['id_usaurio', ['intVal'], [1, 11], 'ID Usuario', ['optional']],
					['email', ['email'], [1, 100], 'Email'],
					['nombre', ['stringType'], [1, 100], 'Nombre'],
					['apellidos', ['stringType'], [1, 100], 'Apellidos'],
					['puesto', ['stringType'], [null, 100], 'Puesto'],
					['role', ['alnum'], [4, 5], 'Role'],
				]
			));

		$this->delete('', 'App\Controller\EditionController:removeUser')
			->setName('admin.remove.users')
			->add(new V(
				$c,
				[
					['id_usuario', ['intVal'], [1, 11], 'ID Usuario'],
				]
			));

		$this->get('', 'App\Controller\EditionController:getUser')->setName('get.user');

	});

});

$app->group('/usuario', function () use ($c) {

	$this->post('/recuperar-password', 'App\Controller\MailerController:recoverUserPassword')
		->setName('recover.user.password')
		->add(new V(
			$c,
			[
				['email', ['email'], [1, 100], 'Email'],
			]
		));

	// Due to the behaviour of browsers when sending PUT or DELETE request, you must add the OPTIONS method. Read about preflight.
	$this->map(['PUT', 'OPTIONS'], '/{id_usuario:\d+}', 'App\Controller\UsersController:updateUser')
		->setName('update.user')
		->add(new V(
			$c,
			[
				['id_usuario', ['intVal'], [1, 11], 'ID Usuario'],
				['nombre', ['stringType'], [2, 50], 'Nombre'],
				['apellidos', ['stringType'], [2, 50], 'Apellidos'],
				['puesto', ['stringType', 'notEmpty'], [null, 100], 'Puesto'],
				['email', ['email'], [null, 100], 'Email'],
			]
		));

	$this->map(['PUT', 'OPTIONS'], '/{id_usuario:\d+}/cambia-contrasena', 'App\Controller\UsersController:updateUserPassword')
		->setName('update.user.password')
		->add(new V(
			$c,
			[
				['vieja_contrasena', ['stringType', 'notEmpty'], [1, 32], 'Vieja contraseña'],
				['nueva_contrasena', ['stringType', 'notEmpty'], [6, 32], 'Nueva contraseña'],
			]
		));

	$this->get('/{email}', 'App\Controller\UsersController:getUser')
		->setName('get.userByEmail')
		->add(new V(
			$c,
			[
				['email', ['email'], [null, 100], 'Email'],
			]
		));

	$this->post('/crear', 'App\Controller\UsersController:createUser')
		->setName('create.user')
		->add(new V(
			$c,
			[
				['nombre', ['stringType'], [2, 50], 'Nombre'],
				['apellidos', ['stringType'], [2, 50], 'Apellidos'],
				['puesto', ['stringType'], [null, 100], 'Puesto'],
				['email', ['email'], [null, 100], 'Email'],
				['role', ['alpha'], [1, 5], 'Rol'],
				['contrasena', ['stringType', 'notEmpty'], [1, 32], 'Contraseña'],
			]
		));

	$this->get('/{email:\d+}', 'App\Controller\UsersController:checkCurrentUser')
		->setName('check.user')
		->add(new V(
			$c,
			[
				['email', ['email'], [null, 100], 'Email'],
			]
		));

});

$app->group('/alumno', function () use ($c) {

	// Due to the behaviour of browsers when sending PUT or DELETE request, you must add the OPTIONS method. Read about preflight.
	$this->map(['PUT', 'OPTIONS'], '/{id_alumno:\d+}', 'App\Controller\StudentsController:updateStudent')
		->setName('update.student')
		->add(new V(
			$c,
			[
				['id_alumno', ['intVal'], [1, 11], 'ID Alumno'],
				['nombre', ['stringType'], [2, 50], 'Nombre'],
				['apellidos', ['stringType'], [2, 50], 'Apellidos'],
				['codigo', ['intVal'], [9, 11], 'Código'],
				['email', ['email'], [null, 100], 'Correo Electronico'],
				['id_programa', ['intVal'], [1, 5], 'Carrera'],
				['id_ciclo', ['intVal'], [1, 5], 'Generación'],
			]
		));

	$this->map(['PUT', 'OPTIONS'], '/{id_alumno:\d+}/cambia-contrasena', 'App\Controller\StudentsController:updateStudentPassword')
		->setName('update.student.password')
		->add(new V(
			$c,
			[
				['vieja_contrasena', ['stringType', 'notEmpty'], [1, 32], 'Vieja contraseña'],
				['nueva_contrasena', ['stringType', 'notEmpty'], [6, 32], 'Nueva contraseña'],
			]
		));

	$this->get('/{codigo}', 'App\Controller\StudentsController:getStudent')
		->setName('get.studentByCode')
		->add(new V(
			$c,
			[
				['codigo', ['intVal'], [1, 11], 'Codigo'],
			]
		));

});

$app->group('/public', function () use ($c) {

	$this->post('/alumno/login', 'App\Controller\SessionController:studentLogin')->setName('session.student.login')
		->add(new V(
			$c,
			[
				['codigo', ['intVal'], [9, 11], 'Código'],
				['contrasena', ['stringType', 'notEmpty'], [6, 32], 'Contraseña'],
			]
		));

	$this->post('/usuario/login', 'App\Controller\SessionController:userLogin')->setName('session.user.login')
		->add(new V(
			$c,
			[
				['email', ['email'], [5, 100], 'Email'],
				['contrasena', ['stringType', 'notEmpty'], [6, 32], 'Contraseña'],
			]
		));

	$this->post('/alumno/recuperar-password', 'App\Controller\MailerController:recoverStudentPassword')
		->setName('recover.student.password')
		->add(new V(
			$c,
			[
				['codigo', ['intVal'], [1, 11], 'Codigo'],
			]
		));

	$this->get('/listados/departamentos', 'App\Controller\PublicListsController:getAllDepartaments')->setName('lists.departaments');
	$this->get('/listados/divisiones', 'App\Controller\PublicListsController:getAllDivisions')->setName('lists.divisions');
	$this->get('/listados/campus', 'App\Controller\PublicListsController:getAllCampus')->setName('lists.campus');
	$this->get('/listados/carreras', 'App\Controller\PublicListsController:getAllPrograms')->setName('lists.careers');
	$this->get('/listados/ciclos', 'App\Controller\PublicListsController:getAllCycles')->setName('lists.cycles');

	$this->get('/consulta/alumno/{codigo}', 'App\Controller\PublicListsController:checkCurrentStudent')->setName('check.student');
	$this->get('/consulta/programa/{clave}', 'App\Controller\PublicListsController:checkCurrentProgram')->setName('check.program');
	$this->get('/consulta/materia/{clave}', 'App\Controller\PublicListsController:checkSubjectExistence')->setName('check.subject');
	$this->get('/listados/departamento/{id_departamento:\d+}/carreras', 'App\Controller\PublicListsController:getAllProgramsByDepartament')
		->setName('lists.programs')
		->add(new V(
			$c,
			[
				['id_departamento', ['intVal'], [1, 11], 'Departamento'],
			]
		));

	$this->post('/crear/alumno', 'App\Controller\StudentsController:createStudent')
		->setName('create.student')
		->add(new V(
			$c,
			[
				['nombre', ['alpha'], [2, 50], 'Nombre'],
				['apellidos', ['alpha'], [2, 50], 'Apellidos'],
				['codigo', ['intVal'], [9, 11], 'Código'],
				['correo', ['email'], [null, 100], 'Correo Electronico'],
				['id_programa', ['intVal'], [1, 5], 'Carrera'],
				['id_ciclo', ['intVal'], [1, 5], 'Generación'],
				['contrasena', ['stringType', 'notEmpty'], [4, 32], 'Contrasena'],
			]
		));
});
