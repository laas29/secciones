<?php
namespace App\Model;

class ProgramsModel extends Model {

	public function __construct($container) {
		parent::__construct($container);
	}

	public function getAllPrograms() {

		$sql = 'select * from `PROGRAMAS`;';

		return $this->query($sql, []);

	}

	public function getSubjects($program_id) {

		$sql = 'select DISTINCT M.clave, M.creditos, M.id_materia, M.nombre from `PROGRAMAS-MATERIAS` PM join MATERIAS M using(id_materia) where id_programa = :program_id and M.preregistro_activo = 1;';

		return $this->query($sql, ['program_id' => $program_id]);

	}

	public function getActiveProgramsForPreregistration() {

		$sql = 'select * from `PROGRAMAS` where preregistro_activo = 1;';

		return $this->query($sql, []);

	}

	public function getProgramsByDepartament($departament_id) {

		$sql = 'select * from PROGRAMAS where id_departamento = :id_departamento';

		return $this->query($sql, ['id_departamento' => $departament_id]);

	}

	public function programExists($program_code) {

		$sql = 'select clave from `PROGRAMAS` where clave = :program_code;';

		return $this->query($sql, array('program_code' => $program_code));

	}

}
