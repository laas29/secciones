<?php
namespace App\Model;

class DivisionsModel extends Model {

	public function __construct($container) {
		parent::__construct($container);
	}

	public function getAllDivisions() {

		$sql = 'select * from DIVISIONES';

		return $this->query($sql, []);

	}

}
