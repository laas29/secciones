<?php
namespace App\Model;

class EditionModel extends Model {

	/**
	 * @param $container
	 */
	public function __construct($container) {
		parent::__construct($container);
	}

	// Students

	/**
	 * @param $args
	 * @return mixed
	 */
	public function getStudent($args) {

		$sql = 'select codigo, id_programa, id_generacion, nombre, apellidos, "alumno" as role, email, id_alumno from ALUMNOS where id_alumno = :id_alumno';
		return $this->query($sql, $args);

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function updateStudent($args) {

		$sql = 'update ALUMNOS set nombre=:nombre, apellidos=:apellidos, codigo=:codigo, email=:email, id_programa=:id_programa, id_generacion=:id_generacion where id_alumno=:id_alumno';

		return $this->query($sql, $args);

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function createStudent($args) {

		$sql = 'insert ALUMNOS values (NULL, :nombre, :apellidos, :codigo, :email, :contrasena, :id_programa, :id_generacion)';

		$this->query($sql, $args);

		return $this->getLastInsertedId();

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function deleteStudent($args) {

		/*Eliminando las referencias para evitar tuplas huerfanas*/
		$sql = 'DELETE FROM `PREREGISTROS-MATERIAS` WHERE id_preregistro in (SELECT id_preregistro from PREREGISTROS WHERE id_alumno = :id_alumno)';

		$this->query($sql, $args);

		$sql = 'DELETE FROM `MOVILIDAD` WHERE id_preregistro in (SELECT id_preregistro from PREREGISTROS WHERE id_alumno = :id_alumno)';

		$this->query($sql, $args);

		$sql = 'DELETE FROM `PREREGISTROS` WHERE id_alumno = :id_alumno';

		$this->query($sql, $args);

		$sql = 'DELETE FROM ALUMNOS WHERE id_alumno = :id_alumno;';

		$this->query($sql, $args);

	}

	// Program

	/**
	 * @param $args
	 * @return mixed
	 */
	public function getProgram($args) {

		$sql = 'SELECT * FROM PROGRAMAS WHERE id_programa = :id_programa';

		return $this->query($sql, $args);

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function updateProgram($args) {

		$sql = 'UPDATE PROGRAMAS SET clave = :clave, nombre = :nombre, id_departamento = :id_departamento, preregistro_activo = :preregistro_activo where id_programa = :id_programa;';

		return $this->query($sql, $args);

	}

	public function deleteProgram($args) {

		$sql = 'SELECT * FROM ALUMNOS WHERE id_programa = :id_programa';

		$programas = $this->query($sql, $args);

		if (count($programas)) {

			return 1;

		} else {

			$sql = 'DELETE FROM `PROGRAMAS-MATERIAS` WHERE id_programa = :id_programa';

			$this->query($sql, $args);

			$sql = 'DELETE FROM PROGRAMAS WHERE id_programa = :id_programa';

			$this->query($sql, $args);

			return 0;

		}

	}

	/**
	 * Crea subquery para insercion en tabla de muchos a muchos
	 * con los elementos con cardinalidad uno a muchos
	 * @param $id
	 * @param $ids
	 * @return mixed
	 */
	private function getInsertValues($id, $ids) {

		$insertValues = '(' . intval($id) . ', ' . intval($ids[0]) . ')';
		$length = count($ids);
		for ($i = 1; $i < $length; $i++) {

			$insertValues .= ',(' . $id . ', ' . $ids[$i] . ')';
		}

		return $insertValues;

	}

	/**
	 * Crea subquery para insercion en tabla de muchos a muchos
	 * con los elementos con cardinalidad muchos a uno
	 * @param $ids
	 * @param $id
	 * @return mixed
	 */
	private function getInverseInsertValues($ids, $id) {

		$insertValues = '(' . intval($ids[0]) . ', ' . intval($id) . ')';
		$length = count($ids);
		for ($i = 1; $i < $length; $i++) {

			$insertValues .= ',(' . $ids[$i] . ', ' . $id . ')';
		}

		return $insertValues;

	}

	/**
	 * @param $args
	 */
	public function updateProgramSubjects(&$args) {

		if (isset($args['idsRemovidos'])) {
			$sql = 'DELETE FROM `PROGRAMAS-MATERIAS` WHERE id_programa = ' . intval($args['id_programa']) . ' and id_materia IN (' . join(',', $args['idsRemovidos']) . ')';

			$this->query($sql, []);
			unset($args['idsRemovidos']);
		}

		if (isset($args['idsElegidos'])) {
			$sql = 'INSERT `PROGRAMAS-MATERIAS` VALUES ' . $this->getInsertValues($args['id_programa'], $args['idsElegidos']);

			$this->query($sql, []);
			unset($args['idsElegidos']);
		}

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function createProgram($args) {

		$sql = 'INSERT PROGRAMAS VALUES (NULL, :clave, :nombre, :id_departamento, :preregistro_activo);';

		$this->query($sql, $args);

		return $this->getLastInsertedId();

	}

	// Cycle

	/**
	 * @param $args
	 * @return mixed
	 */
	public function getCycle($args) {

		$sql = 'SELECT * FROM CICLOS WHERE id_ciclo = :id_ciclo';

		return $this->query($sql, $args);

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function updateCycle($args) {

		$sql = 'UPDATE CICLOS SET clave = :clave, nombre = :nombre, preregistro_activo = :preregistro_activo where id_ciclo = :id_ciclo';

		return $this->query($sql, $args);

	}

	// solo puede haber un único ciclo activo para preregistro
	/**
	 * @return mixed
	 */
	public function disableAllPreRegisterCycles() {

		$sql = 'UPDATE CICLOS SET preregistro_activo = 0 WHERE preregistro_activo = 1;';

		return $this->query($sql, []);

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function createCycle($args) {

		$sql = 'INSERT CICLOS VALUES (NULL, :clave, :nombre, :preregistro_activo);';

		return $this->query($sql, $args);

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function deleteCycle($args) {

		$sql = 'SELECT id_alumno FROM ALUMNOS WHERE id_generacion = :id_ciclo';

		$alumnos = $this->query($sql, $args);

		if (count($alumnos)) {

			return 1;

		}

		$sql = 'SELECT id_preregistro FROM PREREGISTROS WHERE id_ciclo = :id_ciclo';

		$preregistros = $this->query($sql, $args);

		if (count($preregistros)) {

			return 2;

		}

		$sql = 'DELETE FROM CICLOS WHERE id_ciclo = :id_ciclo';

		$this->query($sql, $args);

		return 0;

	}

	// Center

	/**
	 * @param $args
	 * @return mixed
	 */
	public function getCenter($args) {

		$sql = 'SELECT * FROM CENTROS WHERE id_centro = :id_centro;';

		return $this->query($sql, $args);

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function updateCenter($args) {

		$sql = 'UPDATE CENTROS SET nombre = :nombre, clave = :clave WHERE id_centro = :id_centro';

		return $this->query($sql, $args);

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function createCenter($args) {

		$sql = 'INSERT CENTROS VALUES (NULL, :nombre, :clave);';

		return $this->query($sql, $args);

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function deleteCenter($args) {

		$sql = 'DELETE FROM MOVILIDAD WHERE id_centro = :id_centro;';

		$this->query($sql, $args);

		$sql = 'DELETE FROM CENTROS WHERE id_centro = :id_centro;';

		$this->query($sql, $args);

	}

	// Departament

	/**
	 * @param $args
	 * @return mixed
	 */
	public function getDepartament($args) {

		$sql = 'SELECT * FROM DEPARTAMENTOS where id_departamento = :id_departamento;';

		return $this->query($sql, $args);

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function updateDepartament($args) {

		$sql = 'UPDATE DEPARTAMENTOS SET nombre = :nombre, clave = :clave, id_division = :id_division WHERE id_departamento = :id_departamento';

		return $this->query($sql, $args);

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function createDepartament($args) {

		$sql = 'INSERT DEPARTAMENTOS VALUES (NULL, :nombre, :clave, :id_division);';

		return $this->query($sql, $args);

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function deleteDepartament($args) {

		$sql = 'DELETE DEPARTAMENTOS WHERE id_departamento = :id_departamento';

		return $this->query($sql, $args);

	}

	// Subject

	/**
	 * @param $args
	 * @return mixed
	 */
	public function getSubject($args) {

		$sql = 'SELECT * FROM MATERIAS WHERE id_materia = :id_materia;';
		/*
		$sql = 'SELECT
		DISTINCT
		M.id_materia,
		M.clave clave,
		M.nombre nombre,
		M.creditos,
		M.carga_hor_pra practica,
		M.carga_hor_teo teoria,
		M.tipo,
		M.nivel,
		P.id_programa,
		P.nombre carrera
		FROM
		MATERIAS as M
		JOIN `PROGRAMAS-MATERIAS` as PM on
		M.id_materia = PM.id_materia
		JOIN PROGRAMAS as P on
		PM.id_programa = P.id_programa
		WHERE M.id_materia = :id_materia';
		 */

		return $this->query($sql, $args);

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function updateSubject($args) {

		$sql = 'UPDATE MATERIAS SET subj = :subj, clave = :clave, nombre = :nombre, creditos = :creditos, carga_hor_teo = :carga_hor_teo, carga_hor_pra = :carga_hor_pra, tipo = :tipo, nivel = :nivel, preregistro_activo = :preregistro_activo WHERE id_materia = :id_materia';

		return $this->query($sql, $args);

	}

	/**
	 * @param $args
	 */
	public function updateSubjectPrograms(&$args) {

		if (isset($args['idsRemovidos'])) {
			$sql = 'DELETE FROM `PROGRAMAS-MATERIAS` WHERE id_materia = ' . intval($args['id_materia']) . ' and id_programa IN (' . join(',', $args['idsRemovidos']) . ')';

			$this->query($sql, []);
			unset($args['idsRemovidos']);
		}

		if (isset($args['idsElegidos'])) {

			$sql = 'INSERT `PROGRAMAS-MATERIAS` VALUES ' . $this->getInverseInsertValues($args['idsElegidos'], $args['id_materia']);

			$this->query($sql, []);
			unset($args['idsElegidos']);
		}

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function createSubject($args) {

		$sql = 'INSERT MATERIAS VALUES (NULL, :subj, :clave, :nombre, :creditos, :carga_hor_teo, :carga_hor_pra, :tipo, :nivel, :preregistro_activo);';

		$this->query($sql, $args);

		return $this->getLastInsertedId();

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function deleteSubject($args) {

		$sql = 'SELECT * FROM `PREREGISTROS-MATERIAS` WHERE id_materia = :id_materia';

		$preregistrosMaterias = $this->query($sql, $args);

		if (count($preregistrosMaterias)) {

			return 1;

		} else {

			$sql = 'DELETE FROM MATERIAS WHERE id_materia = :id_materia';

			$this->query($sql, $args);

			return 0;

		}

	}

	// User

	/**
	 * @param $args
	 * @return mixed
	 */
	public function getUser($args) {

		$sql = 'SELECT id_usuario, email, nombre, apellidos, puesto, role FROM USUARIOS WHERE id_usuario = :id_usuario;';

		return $this->query($sql, $args);

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function updateUser($args) {

		$sql = 'UPDATE USUARIOS SET email = :email, nombre = :nombre, apellidos = :apellidos, puesto = :puesto, role = :role WHERE id_usuario = :id_usuario';

		return $this->query($sql, $args);

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function createUser($args) {

		$sql = 'INSERT USUARIOS VALUES ( NULL, :email, :nombre, :apellidos, :puesto, :contrasena, :role );';

		return $this->query($sql, $args);

	}

	/**
	 * @param $args
	 * @return mixed
	 */
	public function deleteUser($args) {

		$sql = 'DELETE FROM USUARIOS WHERE id_usuario = :id_usuario';

		return $this->query($sql, $args);

	}

	// Preregistration

	/**
	 * @param $args
	 * @return mixed
	 */
	public function deletePreregistration($args) {

		$sql = 'SELECT * FROM `PREREGISTROS` P JOIN CICLOS C WHERE C.id_ciclo = :id_ciclo and C.preregistro_activo = 1';

		$preregistros = $this->query($sql, $args);

		if (count($preregistros)) {

			return 1;

		} else {

			$sql = 'DELETE FROM `PREREGISTROS-MATERIAS` WHERE id_preregistro IN (SELECT id_preregistro FROM PREREGISTROS WHERE id_ciclo = :id_ciclo)';

			$this->query($sql, $args);

			$sql = 'DELETE FROM `MOVILIDAD` WHERE id_preregistro IN (SELECT id_preregistro FROM PREREGISTROS WHERE id_ciclo = :id_ciclo)';

			$this->query($sql, $args);

			$sql = 'DELETE FROM `PREREGISTROS` WHERE id_ciclo = :id_ciclo';

			$this->query($sql, $args);

			return 0;

		}

	}

}
