<?php
namespace App\Model;

class PreregistrationModel extends Model {

	public function __construct($container) {
		parent::__construct($container);
	}

	public function getPreregistration($row) {

		$sql = 'select * from PREREGISTROS where id_alumno = :id_alumno and id_ciclo = :id_ciclo';

		return $this->query($sql, $row);

	}

	public function createPreregistration($row) {

		$sql = 'insert PREREGISTROS values(null, :id_alumno, :id_ciclo)';

		return $this->query($sql, $row);

	}

	public function createPreregistrationSubjects($rows) {

		$sql = 'insert `PREREGISTROS-MATERIAS` (id_preregistro, id_materia, situacion, horario)';

		return $this->insertMulti($sql, $rows);

	}

	public function updatePreregistrationSubjects($preregistro, $rows) {

		$sql = 'delete from `PREREGISTROS-MATERIAS` where id_preregistro = :id_preregistro';

		$this->query($sql, $preregistro);

		return $this->createPreregistrationSubjects($rows);

	}

	public function createPreregistrationMobility($rows) {

		$sql = 'insert `MOVILIDAD` (clave, id_centro, id_preregistro)';

		return $this->insertMulti($sql, $rows);

	}

	public function updatePreregistrationMobility($preregistro, $rows) {

		$this->deletePreregistrationMobility($preregistro);

		return $this->createPreregistrationMobility($rows);

	}

	public function deletePreregistrationMobility($preregistro) {

		$sql = 'delete from `MOVILIDAD` where id_preregistro = :id_preregistro';

		$this->query($sql, $preregistro);

	}

	public function deletePreregistrationSubjects($preregistro) {

		$sql = 'delete from `PREREGISTROS-MATERIAS` where id_preregistro = :id_preregistro';

		$this->query($sql, $preregistro);

	}

	public function getSubjectsOfStudent($id_student) {

		$sql =
			'select distinct M.id_materia, M.clave, M.nombre, PM.situacion, PM.horario, creditos from
            PREREGISTROS P join `PREREGISTROS-MATERIAS` PM on
            P.id_preregistro = PM.id_preregistro  join CICLOS C on
            P.id_ciclo = C.id_ciclo join MATERIAS M on PM.id_materia = M.id_materia
            where
            P.id_alumno = :id_alumno
            and C.preregistro_activo = 1  ';

		return $this->query($sql, ['id_alumno' => $id_student]);

	}

	public function getMobilityOfStudent($id_student) {

		$sql = 'select
                distinct CE.nombre,
                MO.clave, CE.id_centro
            from
                PREREGISTROS P join `MOVILIDAD` MO on
                P.id_preregistro = MO.id_preregistro
            JOIN CICLOS C on
                P.id_ciclo = C.id_ciclo
            JOIN CENTROS CE on
                MO.id_centro = CE.id_centro
            WHERE
                P.id_alumno = :id_alumno
                AND C.preregistro_activo = 1';

		return $this->query($sql, ['id_alumno' => $id_student]);

	}

	public function getPreregistrationPerProgram($id_program) {

		$sql = 'select
                count( M.id_materia ) as cupos,
                M.clave as materia_clave,
                M.nombre as materia_nombre,
                M.creditos as materia_creditos
            from
                PREREGISTROS P join `PREREGISTROS-MATERIAS` PM on
                P.id_preregistro = PM.id_preregistro join CICLOS C on
                P.id_ciclo = C.id_ciclo join MATERIAS M on
                PM.id_materia = M.id_materia join ALUMNOS A on
                P.id_alumno = A.id_alumno join PROGRAMAS PG on
                PG.id_programa = A.id_programa
            where
                A.id_programa = 91 or A.id_programa = 90
            GROUP by
                materia_clave;';

	}

}
