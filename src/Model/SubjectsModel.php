<?php
namespace App\Model;

class SubjectsModel extends Model {

	/**
	 * @param $container
	 */
	public function __construct($container) {
		parent::__construct($container);
	}

	/**
	 * @param $User_code
	 * @return mixed
	 */
	public function subjectExists($clave) {

		$sql = 'select clave from `MATERIAS` where clave = :clave;';

		return $this->query($sql, array('clave' => $clave));

	}

}
