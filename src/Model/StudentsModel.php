<?php
namespace App\Model;

class StudentsModel extends Model {

	/**
	 * @param $container
	 */
	public function __construct($container) {
		parent::__construct($container);
	}

	/**
	 * @param $student_code
	 * @return mixed
	 */
	public function studentExists($student_code) {

		$sql = 'select codigo from `ALUMNOS` where codigo = :student_code;';

		return $this->query($sql, array(':student_code' => $student_code));

	}

	/**
	 * @param $row
	 * @return mixed
	 */
	public function createStudent($row) {

		$sql = 'insert ALUMNOS values (NULL, :nombre, :apellidos, :codigo, :correo, :contrasena, :id_programa, :id_generacion)';

		return $this->query($sql, $row);

	}

	/**
	 * @param $student_code
	 * @return mixed
	 */
	public function getStudentCredentials($student_code) {
		$sql = 'select codigo, contrasena, id_programa, id_generacion, nombre, apellidos, "alumno" as role, email, id_alumno from ALUMNOS where codigo = :student_code';
		return $this->query($sql, [':student_code' => $student_code]);
	}

	/**
	 * @param $row
	 * @return mixed
	 */
	public function updateStudent($row) {

		$sql = 'update ALUMNOS set nombre=:nombre, apellidos=:apellidos, codigo=:codigo, email=:email, id_programa=:id_programa, id_generacion=:id_generacion where id_alumno=:id_alumno';

		return $this->query($sql, $row);

	}

	/**
	 * @param $row
	 * @return mixed
	 */
	public function updatePassword($row) {

		$sql = 'update ALUMNOS set contrasena = :nueva_contrasena where id_alumno = :id_alumno';

		return $this->query($sql, $row);

	}

}
