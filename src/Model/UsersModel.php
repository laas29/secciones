<?php
namespace App\Model;

class UsersModel extends Model {

	/**
	 * @param $container
	 */
	public function __construct($container) {
		parent::__construct($container);
	}

	/**
	 * @param $User_code
	 * @return mixed
	 */
	public function userExists($email) {

		$sql = 'select email from `USUARIOS` where email = :email;';

		return $this->query($sql, array('email' => $email));

	}

	/**
	 * @param $row
	 * @return mixed
	 */
	public function createUser($row) {

		$sql = 'insert USUARIOS values (NULL, :nombre, :apellidos, :puesto, :email, :contrasena, :id_programa, :id_generacion)';

		return $this->query($sql, $row);

	}

	/**
	 * @param $codigo
	 * @return mixed
	 */
	public function getUserCredentials($email) {
		// $sql = 'select codigo, contrasena, id_programa, id_generacion, nombre, apellidos, "alumno" as role, email, id_alumno from ALUMNOS where codigo = :User_code';
		$sql = 'select * from USUARIOS where email = :email';
		return $this->query($sql, [':email' => $email]);
	}

	/**
	 * @param $row
	 * @return mixed
	 */
	public function updateUser($row) {

		$sql = 'update USUARIOS set nombre=:nombre, apellidos=:apellidos, puesto=:puesto, email=:email where id_usuario=:id_usuario';

		return $this->query($sql, $row);

	}

	/**
	 * @param $row
	 * @return mixed
	 */
	public function updatePassword($row) {

		$sql = 'update USUARIOS set contrasena = :nueva_contrasena where id_usuario = :id_usuario';

		return $this->query($sql, $row);

	}

}
