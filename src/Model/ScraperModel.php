<?php
namespace App\Model;

class ScraperModel extends Model
{

    public function __construct($container)
    {
        parent::__construct($container);
    }

    public function getAllPrograms()
    {

        $sql = 'select * from `PROGRAMAS`;';

        return $this->query($sql, []);

    }

    public function setSubject($row)
    {

        $sql = 'insert MATERIAS values(
            null,
            :subj,
            :clave,
            :nombre,
            :creditos,
            :carga_hor_teo,
            :carga_hor_pra,
            :tipo,
            :nivel
        )';

        /*
        "' . $row['clave'] . '",
        "' . $row['nombre'] . '",
        "' . $row['creditos'] . '",
        "' . $row['carga_hor_teo'] . '",
        "' . $row['carga_hor_pra'] . '",
        "' . $row['tipo'] . '",
        "' . $row['area'] . '",
        "' . $row['TS'] . '",
        "' . $row['LI'] . '",
        "' . $row['MA'] . '",
        "' . $row['DO'] . '"
         */

        return $this->query($sql, $row);

    }

    public function setProgramSubject($row)
    {

        $sql = 'insert `PROGRAMAS-MATERIAS` values(
            :id_programa,
            :id_materia
        )';

        return $this->query($sql, $row);

    }

    public function getSubject($row)
    {

        $sql = 'select * from MATERIAS where clave like :clave;';
        return $this->query($sql, $row);

    }

}

/*

[237] => SUBJ,CLAVE,MATERIA,CRED,TEORIA,PRACTICA,TIPO,NIVEL,PRERREQUISITOS,CORREQUISITOS,CARRERAS,AREA,DEPTO
[238] => Array
(
[0] => "CICO"
[1] => "CC316"
[2] => "ANALISIS Y DISE~O DE ALGORITMOS"
[3] => 11
[4] => 80
[5] => 0
[6] => " C"
[7] => " LI"
[8] => "S"
[9] => " CICO CC209"
[10] =>
[11] => "ES"
[12] => "T-1010
[13] => D-1500
[14] => G-3141
[15] => H-3354
[16] => I-3805
[17] => U-8020"
)

[244] => Array
(
[0] => "CICO"
[1] => "CC204"
[2] => "ESTRUCTURA DE ARCHIVOS"
[3] => 11
[4] => 80
[5] => 0
[6] => " C"
[7] => " LI TS"
[8] => "S"
[9] => " CICO CC202"
[10] =>
[11] => "BPO"
[12] => "T-1010
[13] => D-1500
[14] => G-3181
[15] => H-3352
[16] => K-3522
[17] => I-3805
[18] => M-7322
[19] => U-8020"
)

 */
