<?php
namespace App\Model;

class DepartamentsModel extends Model {

	public function __construct($container) {
		parent::__construct($container);
	}

	public function getAllDepartaments() {

		$sql = 'select * from DEPARTAMENTOS';

		return $this->query($sql, []);

	}

}
