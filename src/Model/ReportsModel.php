<?php
namespace App\Model;

class ReportsModel extends Model
{

    /**
     * @param $container
     */
    public function __construct($container)
    {
        parent::__construct($container);
    }

    /**
     * @param $numPrograms
     * @param $fieldname
     * @return mixed
     */
    private function getProgramsSubquery($numPrograms, $fieldname)
    {

        $subQuery = ' ' . $fieldname . ' = ? ';

        for ($i = 1; $i < $numPrograms; $i++) {
            $subQuery .= ' OR ' . $fieldname . ' = ? ';
        }

        return $subQuery;

    }

    /**
     * @param $args
     * @return mixed
     */
    public function getPreregistrationPerPrograms($args)
    {

        $subQuery = $this->getProgramsSubquery(count($args['carreras']), 'A.id_programa');

        $sql = 'select distinct
                M.clave,
                M.nombre,
                PM.horario,
                count( PM.id_materia ) as cupos
            from
                PREREGISTROS P join `PREREGISTROS-MATERIAS` PM on
                P.id_preregistro = PM.id_preregistro join CICLOS C on
                P.id_ciclo = C.id_ciclo join MATERIAS M on
                PM.id_materia = M.id_materia join ALUMNOS A on
                P.id_alumno = A.id_alumno join PROGRAMAS PG on
                PG.id_programa = A.id_programa
            where
                C.id_ciclo = ?
                AND (' . $subQuery . ')
            GROUP by
                clave, nombre, horario';

        return $this->query($sql, array_merge([$args['id_ciclo']], $args['carreras']));

    }

    /**
     * @param $args
     * @return mixed
     */
    public function getPreregistrationMobilityPerPrograms($args)
    {

        $subQuery = $this->getProgramsSubquery(count($args['carreras']), 'A.id_programa');

        $sql = 'select
                MO.clave,
                CE.nombre as campus,
                count( MO.id_movilidad ) as cupos,
                CE.id_centro
            from
                PREREGISTROS P join CICLOS C on
                P.id_ciclo = C.id_ciclo join ALUMNOS A on
                P.id_alumno = A.id_alumno join PROGRAMAS PG on
                PG.id_programa = A.id_programa join MOVILIDAD MO on
                MO.id_preregistro = P.id_preregistro join CENTROS CE on
                MO.id_centro = CE.id_centro
            where
                C.id_ciclo = ?
                AND (' . $subQuery . ')

            GROUP by
                MO.clave, CE.nombre, CE.id_centro';

        return $this->query($sql, array_merge([$args['id_ciclo']], $args['carreras']));

    }

    /**
     * @param $args
     * @return mixed
     */
    public function getStudentsByCicleSubjectAndProgram($args)
    {

        $subQuery = $this->getProgramsSubquery(count($args['carreras']), 'A.id_programa');

        $sql = 'select distinct
            PM.situacion,
            A.nombre, A.apellidos, A.codigo, A.email,
            (SELECT nombre from CICLOS where id_ciclo = A.id_generacion) as generacion,
            PG.nombre as programa,
            PM.horario
        from
            PREREGISTROS P join `PREREGISTROS-MATERIAS` PM on
            P.id_preregistro = PM.id_preregistro join CICLOS C on
            P.id_ciclo = C.id_ciclo join MATERIAS M on
            PM.id_materia = M.id_materia join ALUMNOS A on
            P.id_alumno = A.id_alumno join PROGRAMAS PG on
            PG.id_programa = A.id_programa
        where
            C.id_ciclo = ? AND
            M.clave = ? AND
            (' . $subQuery . ')';

        return $this->query($sql, array_merge([$args['id_ciclo']], [$args['clave']], $args['carreras']));

    }

    /**
     * @param $args
     * @return mixed
     */
    public function getStudentsMobilityByCicleSubjectAndProgram($args)
    {

        $subQuery = $this->getProgramsSubquery(count($args['carreras']), 'A.id_programa');

        $sql = 'select distinct
            A.nombre, A.apellidos, A.codigo, A.email,
            (SELECT nombre from CICLOS where id_ciclo = A.id_generacion) as generacion,
            PG.nombre as programa,
            CE.nombre as campus
        from
            PREREGISTROS P join CICLOS C on
            P.id_ciclo = C.id_ciclo join ALUMNOS A on
            P.id_alumno = A.id_alumno join PROGRAMAS PG on
            PG.id_programa = A.id_programa join MOVILIDAD MO on
            MO.id_preregistro = P.id_preregistro join CENTROS CE on
            MO.id_centro = CE.id_centro
        where
            C.id_ciclo = ? and
            MO.clave = ? and
            MO.id_centro = ? and
            (' . $subQuery . ')';

        return $this->query($sql, array_merge([$args['id_ciclo']], [$args['clave']], [$args['id_centro']], $args['carreras']));

    }

    /**
     * @param $args
     * @return mixed
     */
    public function getPreregisteredStudents($args)
    {

        $subQuery = $this->getProgramsSubquery(count($args['carreras']), 'A.id_programa');

        $sql = 'select
            distinct A.nombre,
            A.apellidos,
            A.codigo,
            A.email,
            (
                SELECT
                    nombre
                from
                    CICLOS
                where
                    id_ciclo = A.id_generacion
            ) as generacion,
            PG.nombre as programa
        from
            PREREGISTROS P join CICLOS C on
            P.id_ciclo = C.id_ciclo join ALUMNOS A on
            P.id_alumno = A.id_alumno join PROGRAMAS PG on
            PG.id_programa = A.id_programa join MOVILIDAD MO on
            MO.id_preregistro = P.id_preregistro join CENTROS CE on
            MO.id_centro = CE.id_centro
        where
            C.id_ciclo = ?
            AND (' . $subQuery . ')
        UNION select
            distinct A.nombre,
            A.apellidos,
            A.codigo,
            A.email,
            (
                SELECT
                    nombre
                from
                    CICLOS
                where
                    id_ciclo = A.id_generacion
            ) as generacion,
            PG.nombre as programa
        from
            PREREGISTROS P join `PREREGISTROS-MATERIAS` PM on
            P.id_preregistro = PM.id_preregistro join CICLOS C on
            P.id_ciclo = C.id_ciclo join MATERIAS M on
            PM.id_materia = M.id_materia join ALUMNOS A on
            P.id_alumno = A.id_alumno join PROGRAMAS PG on
            PG.id_programa = A.id_programa
        where
            C.id_ciclo = ? AND (' . $subQuery . ')';

        $queryArgs = array_merge([$args['id_ciclo']], $args['carreras'], [$args['id_ciclo']], $args['carreras']);

        return $this->query($sql, $queryArgs);

    }

    /*Reportes para seccion de edicion*/

    /**
     * @param $columns
     * @param $column
     */
    public function validateColumn($columns, $column)
    {
        if (array_search($column, $columns)) {
            return true;
        }
        return false;
    }

    /**
     * @param $key
     * @param $colsWithAlias
     * @return mixed
     */
    public function getRealFieldName($key, $colsWithAlias)
    {

        $columnName = $key;

        if (isset($colsWithAlias[$key])) {
            $columnName = $colsWithAlias[$key];
        }

        return $columnName;

    }

    /**
     * @param $fields
     * @param $allowedColNames
     * @param $colsWithAlias
     * @param array $exactValueFields
     * @return mixed
     */
    public function whereWithLikeBuilder($fields, $allowedColNames, $colsWithAlias, $exactValueFields = [])
    {

        $subQuery = '';
        $i = 0;
        $parameterValues = [];
        $columnName = '';
        $areThereExactValueFieldsToCheck = !!count($exactValueFields);

        if (count($fields)) {

            foreach ($fields as $key => $value) {

                if (!is_null($value) && array_search($key, $allowedColNames) !== false) {

                    $columnName = $this->getRealFieldName($key, $colsWithAlias);

                    if ($i++) {
                        $subQuery .= ' AND ';
                    } else {
                        $subQuery = ' WHERE ';
                    }

                    if ($areThereExactValueFieldsToCheck && array_search($key, $exactValueFields) !== false) {
                        $subQuery .= $columnName . ' = "' . $value . '" ';
                    } else {
                        $subQuery .= $columnName . ' like "%' . $value . '%"';
                    }
                }

            }

        }

        return $subQuery;

    }

    /**
     * @param $args
     * @param $allowedColNames
     * @param $colsWithAlias
     * @return mixed
     */
    public function orderByBuilder($args, $allowedColNames, $colsWithAlias, $defaultOrderBy)
    {

        $subQuery = '';

        if (isset($args['sortBy']) && isset($args['sortOrder'])) {

            $sortBy = $args['sortBy'];
            $sortOrder = $args['sortOrder'];

            if (array_search($sortBy, $allowedColNames) !== false) {

                if (isset($colsWithAlias[$sortBy])) {
                    $sortBy = $colsWithAlias[$sortBy];
                }

                if ($sortOrder == 'desc' || $sortOrder == 'asc') {
                    $subQuery = ' ORDER BY ' . $sortBy . ' ' . $sortOrder;
                }

            }
        } else {
            if ($defaultOrderBy) {
                $subQuery = ' ORDER BY ' . $defaultOrderBy;
            }
        }

        return $subQuery;

    }

    /**
     * @param $args
     * @param $query
     * @param $allowedColNames
     * @param $aliasOfAllowedColNames
     * @param array $exactValueFields
     * @param defaultOrderBy
     * @return array
     */
    public function executeQuery($args, $query, $allowedColNames, $aliasOfAllowedColNames, $exactValueFields = [], $defaultOrderBy = null)
    {

        $subQuery = '';
        if (isset($args['fields'])) {
            $subQuery = $this->whereWithLikeBuilder(
                // todos los campos enviados
                $args['fields'],
                // los campos permitidos
                $allowedColNames,
                // los campos que tienen alias en la consulta
                $aliasOfAllowedColNames,
                // campos que tienen que coincidir exactamente, se usa = en vez de like
                $exactValueFields
            );
        }

        $orderQuery = $this->orderByBuilder($args, $allowedColNames, $aliasOfAllowedColNames, $defaultOrderBy);

        $sql = $query . ' ' . $subQuery . $orderQuery;

        if (isset($args['skip']) && isset($args['limit'])) {
            $sql .= ' LIMIT ' . $args['skip'] . ', ' . $args['limit'];
        }

        $fields = isset($args['fields']) ? $args['fields'] : $args;

        $results = $this->query($sql, $fields);

        $sql = 'select FOUND_ROWS() count;';

        $count = $this->query($sql, []);

        return ['rows' => $results, 'count' => $count[0]['count']];

    }

    /**
     * @param $args
     * @return mixed
     */
    public function getAllStudents($args)
    {

        $allowedColNames = ['id_alumno', 'nombre', 'apellidos', 'codigo', 'email', 'generacion', 'programa'];
        $alias = ['nombre' => 'A.nombre', 'generacion' => 'C.nombre', 'programa' => 'PG.nombre'];

        $sql = 'SELECT SQL_CALC_FOUND_ROWS DISTINCT
            id_alumno, A.nombre, apellidos, codigo, email,
            C.nombre AS generacion,
            PG.nombre AS programa
       FROM
            ALUMNOS A JOIN PROGRAMAS PG ON
            PG.id_programa = A.id_programa
            JOIN CICLOS C ON A.id_generacion = C.id_ciclo
             ';

        return $this->executeQuery($args, $sql, $allowedColNames, $alias);

    }

    /**
     * @param $args
     * @return mixed
     */
    public function getAllPrograms($args)
    {

        $allowedColNames = ['clave', 'nombre', 'departamento', 'preregistro_activo'];
        $alias = ['clave' => 'P.clave', 'nombre' => 'P.nombre', 'departamento' => 'D.nombre'];

        $sql = 'SELECT
            SQL_CALC_FOUND_ROWS
            DISTINCT
            P.id_programa,
            P.clave,
            P.nombre,
            D.nombre as departamento,
            P.preregistro_activo
        from
            PROGRAMAS AS P
        JOIN DEPARTAMENTOS AS D on
            P.id_departamento = D.id_departamento';

        return $this->executeQuery($args, $sql, $allowedColNames, $alias);

    }

    /**
     * @param $args
     * @return mixed
     */
    public function getProgramsBySubject($args)
    {

        $sql = 'SELECT
            DISTINCT *
        FROM
            PROGRAMAS
        JOIN `PROGRAMAS-MATERIAS`
                USING(id_programa)
        WHERE
            id_materia = :id_materia';

        return $this->query($sql, $args);

    }

    /**
     * @param $args
     * @return mixed
     */
    public function getAllCycles($args)
    {

        $allowedColNames = ['clave', 'nombre', 'preregistro'];
        $alias = ['preregistro' => 'preregistro_activo'];

        $sql = 'SELECT
            SQL_CALC_FOUND_ROWS
            DISTINCT
            id_ciclo,
            clave,
            nombre,
            preregistro_activo as preregistro
        FROM
            CICLOS
        ';

        return $this->executeQuery($args, $sql, $allowedColNames, $alias, [], ' nombre desc ');

    }

    /**
     * @param $args
     * @return mixed
     */
    public function getAllCenters($args)
    {

        $allowedColNames = ['clave', 'nombre'];
        $alias = [];

        $sql = 'SELECT
            SQL_CALC_FOUND_ROWS
            DISTINCT
            id_centro,
            clave,
            nombre
        FROM
            CENTROS
        ';

        return $this->executeQuery($args, $sql, $allowedColNames, $alias);
    }

    /**
     * @param $args
     * @return mixed
     */
    public function getAllDepartaments($args)
    {

        $allowedColNames = ['clave', 'nombre', 'division'];
        $alias = ['nombre' => 'DE.nombre', 'division' => 'DI.nombre'];

        $sql = 'SELECT
        SQL_CALC_FOUND_ROWS DISTINCT
        DE.id_departamento,
        DE.nombre,
        clave,
        DI.nombre division
        FROM
        DEPARTAMENTOS DE
        JOIN DIVISIONES DI
        using(id_division)
        ';

        return $this->executeQuery($args, $sql, $allowedColNames, $alias);
    }

    /**
     * @param $args
     * @return mixed
     */
    public function getAllSubjects($args)
    {

        $allowedColNames = ['clave', 'nombre', 'subj', 'creditos', 'teoria', 'practica', 'tipo', 'nivel', 'carrera', 'id_programa', 'preregistro_activo'];
        $alias = ['clave' => 'M.clave', 'nombre' => 'M.nombre', 'teoria' => 'carga_hor_teo', 'practica' => 'carga_hor_pra', 'subj' => 'M.subj', 'id_programa' => 'PM.id_programa', 'preregistro_activo' => 'M.preregistro_activo'];
        $exactValueFields = ['id_programa'];

        $sql = 'SELECT
            SQL_CALC_FOUND_ROWS
            DISTINCT
            M.id_materia,
            M.clave clave,
            M.nombre nombre,
            M.subj,
            M.creditos,
            M.carga_hor_pra practica,
            M.carga_hor_teo teoria,
            M.tipo,
            M.nivel,
            M.preregistro_activo
        FROM
            MATERIAS as M LEFT JOIN `PROGRAMAS-MATERIAS` PM ON M.id_materia = PM.id_materia';

        return $this->executeQuery($args, $sql, $allowedColNames, $alias, $exactValueFields);
    }

    /**
     * @param $args
     * @return mixed
     */
    public function getAllUsers($args)
    {

        $allowedColNames = ['email', 'nombre', 'apellidos', 'puesto', 'role'];
        $alias = [];

        $sql = 'SELECT
            SQL_CALC_FOUND_ROWS
            DISTINCT
            id_usuario, email, nombre, apellidos, puesto, role
            from USUARIOS
        ';

        return $this->executeQuery($args, $sql, $allowedColNames, $alias);
    }

    /**
     * @param $args
     * @return mixed
     */
    public function getAllPreregisterCycles($args)
    {

        $allowedColNames = ['nombre', 'preregistro_activo'];
        $alias = ['nombre' => 'C.nombre', 'preregistro_activo' => 'C.preregistro_activo'];

        $sql = 'SELECT DISTINCT C.id_ciclo, C.nombre, C.preregistro_activo FROM `PREREGISTROS` P JOIN CICLOS C ON P.id_ciclo = C.id_ciclo ';

        return $this->executeQuery($args, $sql, $allowedColNames, $alias);

    }

}
