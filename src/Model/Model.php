<?php
namespace App\Model;

class Model {

	/**
	 * @param $container
	 */
	public function __construct($container) {

		$this->db = $container->get('pdo');
		$this->logger = $container->get('logger');
		$this->respFormatter = $container->get('responseFormatter');

	}

	/**
	 * @return mixed
	 */
	public function getLastInsertedId() {

		// esto puede variar si se usa POSGRES
		return $this->db->lastInsertId();

	}

	/**
	 * @param $sql
	 * @param $params
	 * @return mixed
	 */
	public function query($sql, $params) {

		try {

			$stmt = $this->db->prepare($sql, [\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY]);

			if (empty($params)) {
				$stmt->execute();
			} else {
				$stmt->execute($params);
			}

			// mostrara las queries en la salida
			if ($_ENV['SHOW_QUERIES'] === "true") {

				echo '<br/>' . print_r($params, true) . '<br/>' . \PdoDebugger::show($sql, $params);

				$res = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				print_r($res);

			} else {

				$res = $stmt->fetchAll(\PDO::FETCH_ASSOC);

			}

			return $res;

		} catch (PDOException $ex) {

			$this->logger->addInfo($ex);
			return $this->respFormatter->getJsonResponse($response, 500, [], 'Hubo un error con la base de datos');

		}

	}

	/**
	 * @param $sqls
	 * @param $params
	 * @return mixed
	 */
	public function transaction($sqls, $params) {

		try {

			$this->db->beginTransaction();

			foreach ($sqls as $key => $sql) {

				$stmt = $this->db->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
				$stmt->execute($params[$key]);

			}

			$this->db->commit();

		} catch (PDOException $ex) {

			$this->db->rollBack();
			$this->logger->addInfo($ex);
			return $this->respFormatter->getJsonResponse($response, 500, [], 'Hubo un error en la base de datos');
		}

	}

	/**
	 * @param $sql
	 * @param $rows
	 * @return mixed
	 */
	public function insertMulti($sql, $rows) {

		$elemPerRow = count($rows[0]);

		$sql .= ' values ';
		$tags = '(?';

		for ($i = 1; $i < $elemPerRow; $i++) {
			$tags .= ',?';
		}

		$tags .= ')';

		$sql .= $tags;

		$numValues = count($rows);

		for ($i = 1; $i < $numValues; $i++) {

			$sql .= ', ' . $tags;

		}

		// PHP 5.6+
		$flattenedRows = array_merge(...$rows);

		return $this->query($sql, $flattenedRows);

	}

}
