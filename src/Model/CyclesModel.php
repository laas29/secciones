<?php
namespace App\Model;

class CyclesModel extends Model {

	public function __construct($container) {
		parent::__construct($container);
	}

	public function getAllCycles() {

		$sql = 'select * from `CICLOS` ORDER BY nombre desc;';

		return $this->query($sql, []);

	}

	public function getActiveCycleForPreregistration() {

		$sql = 'select * from CICLOS where preregistro_activo = 1';

		return $this->query($sql, []);

	}

}
