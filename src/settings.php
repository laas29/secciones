<?php
return [
    'settings' => [

        // Access-Control-Allow-Methods
        'determineRouteBeforeAppMiddleware' => true,
        // set to false in production
        'displayErrorDetails' => $_ENV['CURRENT_ENV'] != 'PRODUCTION',

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
        ],

        'pdo' => [
            // cadena de conexion al servidor
            'dsn' => $_ENV['PDO_DSN'],
            'user' => $_ENV['PDO_USER'],
            'psw' => $_ENV['PDO_PSW'],
        ],

        'jwt' => [
            // tiempo de vida de la session debe especificarse
            'session_life' => $_ENV['JWT_LIFE_SPAN'],
            // emisor del token (nuestra url)
            'issuer' => $_ENV['JWT_ISSUER'],
            // receptor del token (url cliente o front end)
            'audience' => $_ENV['JWT_AUDIENCE'],
            // palabra con la que se haran los hashes de los token
            'secret' => $_ENV['JWT_SECRET'],
            // si la aplicación corre en una conexion que no es segura (https), este valor debe ser seteado en false
            'secure' => $_ENV['JWT_SECURE'] == 'true' ? true : false,
        ],

    ],
];
