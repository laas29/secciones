<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);

// Enabling CORS
$c = $app->getContainer();
$app->add(new \App\Middleware\CORSMiddleware($c));
$app->add(new \Slim\Middleware\JwtAuthentication([
	'session_life' => $c->settings['jwt']['session_life'],
	'issuer' => $c->settings['jwt']['issuer'],
	'audience' => $c->settings['jwt']['audience'],
	'secret' => $app->getContainer()->settings['jwt']['secret'],
	'secure' => $c->settings['jwt']['secure'],
	'rules' => [
		new \Slim\Middleware\JwtAuthentication\RequestPathRule([
			// revisa autenticacion JWT en todas las secciones
			'path' => '/',
			// excepto en las secciones publicas
			// (todas las url que empiezan con /public)
			'passthrough' => ['/public'],
		]),
		// todas las peticiones OPTIONS pasaran con o sin token
		new \Slim\Middleware\JwtAuthentication\RequestMethodRule([
			"passthrough" => ["OPTIONS"],
		]),
	],
	'callback' => function ($request, $response, $arguments) use ($c) {

		$loggedInUserRole = $arguments['decoded']->role;

		if (!$c->get('ACLHelper')->checkACLPermissions($request, $loggedInUserRole)) {
			die("No tienes permisos ¯\_(ツ)_/¯");
		}

		// como estaba
		$c['jwtSession'] = $arguments['decoded'];
	},
	"error" => function ($request, $response, $arguments) {
		$data["status"] = "error";
		$data["message"] = $arguments["message"];
		return $response
			->withHeader("Content-Type", "application/json")
			->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
	},
]));
