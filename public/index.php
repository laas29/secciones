<?php
if (PHP_SAPI == 'cli-server') {
	// To help the built-in PHP dev server, check if the request was actually for
	// something which should probably be served as a static file
	$url = parse_url($_SERVER['REQUEST_URI']);
	$file = __DIR__ . $url['path'];
	if (is_file($file)) {
		return false;
	}
}

require __DIR__ . '/../vendor/autoload.php';

// we don't need session cookies when there is JWT
// session_start();

// dont forget to put in git ignore list the .env file
$dotenv = new Dotenv\Dotenv(dirname(dirname(__FILE__)));
// load .env variables
$dotenv->load();
// this variables must be given
$dotenv->required(['PDO_DSN', 'PDO_USER', 'PDO_PSW', 'JWT_SECRET']);
// this variables cannot be empty
$dotenv->required(['PDO_DSN', 'PDO_USER', 'JWT_SECRET'])->notEmpty();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

// Run app
$app->run();
