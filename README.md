# Sistema para el manejo de los cupos por secciones #

### Integrantes ###

* Luis Arias laas29@gmail.com
* Maria Espinosa thedarkwoodscircus@gmail.com

### Instrucciones ###

Para poder hacer uso de la aplicacion correctamente, se necesita configurar las variables de entorno ".env".
Junto con el codigo del proyecto se incluye un archivo de ejemplo (.env.example) con cada una de las variables
de entorno requeridas para hacer funcionar la aplicación. 

La base de datos la encuentra en la carpeta db
