-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.13-MariaDB - Source distribution
-- SO del servidor:              Linux
-- HeidiSQL Versión:             9.4.0.5169
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para secciones
CREATE DATABASE IF NOT EXISTS `secciones` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `secciones`;

-- Volcando estructura para tabla secciones.ALUMNOS
CREATE TABLE IF NOT EXISTS `ALUMNOS` (
  `id_alumno` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `codigo` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `contrasena` varchar(64) DEFAULT NULL,
  `id_programa` int(11) DEFAULT NULL,
  `id_generacion` int(11) NOT NULL,
  UNIQUE KEY `codigo` (`codigo`),
  KEY `PK_ALUMNOS` (`id_alumno`),
  KEY `FK_ALUMNOS_CICLOS` (`id_generacion`),
  KEY `FK_ALUMNOS_PROGRAMAS` (`id_programa`),
  CONSTRAINT `FK_ALUMNOS_PROGRAMAS` FOREIGN KEY (`id_programa`) REFERENCES `PROGRAMAS` (`id_programa`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla secciones.CENTROS
CREATE TABLE IF NOT EXISTS `CENTROS` (
  `id_centro` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `clave` varchar(10) NOT NULL,
  KEY `PK_CENTROS` (`id_centro`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla secciones.CICLOS
CREATE TABLE IF NOT EXISTS `CICLOS` (
  `id_ciclo` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(10) DEFAULT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `preregistro_activo` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Indica el si esta activo para pre-registro',
  KEY `PK_CICLOS` (`id_ciclo`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla secciones.COORDINACIONES
CREATE TABLE IF NOT EXISTS `COORDINACIONES` (
  `id_coordinacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `id_coordinador` int(11) NOT NULL COMMENT 'Usuario de tipo ''Coordinador'' a cargo',
  KEY `PK_COORDINACIONES` (`id_coordinacion`),
  KEY `FK_COORDINACIONES_USUARIOS` (`id_coordinador`),
  CONSTRAINT `FK_COORDINACIONES_USUARIOS` FOREIGN KEY (`id_coordinador`) REFERENCES `USUARIOS` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla secciones.COORDINACIONES-SECCIONES
CREATE TABLE IF NOT EXISTS `COORDINACIONES-SECCIONES` (
  `id_coordinacion` int(11) NOT NULL,
  `id_seccion` int(11) NOT NULL,
  `cupos_necesarios` int(11) DEFAULT NULL,
  KEY `PK_COORDINACIONES-SECCIONES` (`id_coordinacion`,`id_seccion`),
  KEY `FK_COORDINACIONES-SECCIONES_SECCIONES` (`id_seccion`),
  CONSTRAINT `FK_COORDINACIONES-SECCIONES_COORDINACIONES` FOREIGN KEY (`id_coordinacion`) REFERENCES `COORDINACIONES` (`id_coordinacion`),
  CONSTRAINT `FK_COORDINACIONES-SECCIONES_SECCIONES` FOREIGN KEY (`id_seccion`) REFERENCES `SECCIONES` (`id_seccion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla secciones.DEPARTAMENTOS
CREATE TABLE IF NOT EXISTS `DEPARTAMENTOS` (
  `id_departamento` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `clave` varchar(10) DEFAULT NULL,
  `id_division` int(11) NOT NULL,
  KEY `PK_DEPARTAMENTOS` (`id_departamento`),
  KEY `FK_DEPARTAMENTOS_DIVISIONES` (`id_division`),
  CONSTRAINT `FK_DEPARTAMENTOS_DIVISIONES` FOREIGN KEY (`id_division`) REFERENCES `DIVISIONES` (`id_division`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla secciones.DIVISIONES
CREATE TABLE IF NOT EXISTS `DIVISIONES` (
  `id_division` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  KEY `PK_DIVISION` (`id_division`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla secciones.HORARIOS
CREATE TABLE IF NOT EXISTS `HORARIOS` (
  `id_horario` int(11) NOT NULL AUTO_INCREMENT,
  `dia` tinyint(4) DEFAULT NULL,
  `hora_inicio` tinyint(4) DEFAULT NULL,
  `hora_fin` tinyint(4) DEFAULT NULL,
  `id_seccion` int(11) NOT NULL,
  KEY `PK_HORARIOS` (`id_horario`),
  KEY `FK_HORARIO_SECCIONES` (`id_seccion`),
  CONSTRAINT `FK_HORARIO_SECCIONES` FOREIGN KEY (`id_seccion`) REFERENCES `SECCIONES` (`id_seccion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla secciones.MATERIAS
CREATE TABLE IF NOT EXISTS `MATERIAS` (
  `id_materia` int(11) NOT NULL AUTO_INCREMENT,
  `subj` varchar(10) DEFAULT NULL,
  `clave` varchar(10) DEFAULT NULL,
  `nombre` varchar(150) DEFAULT NULL,
  `creditos` tinyint(2) DEFAULT NULL,
  `carga_hor_teo` tinyint(3) DEFAULT NULL,
  `carga_hor_pra` tinyint(3) DEFAULT NULL,
  `tipo` varchar(15) DEFAULT NULL,
  `nivel` varchar(15) DEFAULT NULL,
  `preregistro_activo` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Indica si estara disponible para preregistro',
  UNIQUE KEY `clave` (`clave`),
  KEY `PK_MATERIAS` (`id_materia`)
) ENGINE=InnoDB AUTO_INCREMENT=5524 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla secciones.MOVILIDAD
CREATE TABLE IF NOT EXISTS `MOVILIDAD` (
  `id_movilidad` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(10) DEFAULT NULL,
  `id_centro` int(11) NOT NULL,
  `id_preregistro` int(11) NOT NULL,
  KEY `PK_MOVILIDAD` (`id_movilidad`),
  KEY `FK_MOVILIDAD_CENTROS` (`id_centro`),
  KEY `FK_MOVILIDAD_PREREGISTROS` (`id_preregistro`),
  CONSTRAINT `FK_MOVILIDAD_CENTROS` FOREIGN KEY (`id_centro`) REFERENCES `CENTROS` (`id_centro`),
  CONSTRAINT `FK_MOVILIDAD_PREREGISTROS` FOREIGN KEY (`id_preregistro`) REFERENCES `PREREGISTROS` (`id_preregistro`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla secciones.PREREGISTROS
CREATE TABLE IF NOT EXISTS `PREREGISTROS` (
  `id_preregistro` int(11) NOT NULL AUTO_INCREMENT,
  `id_alumno` int(11) NOT NULL,
  `id_ciclo` int(11) NOT NULL,
  KEY `PK_PREREGISTROS` (`id_preregistro`),
  KEY `FK_PREREGISTROS_ALUMNOS` (`id_alumno`),
  KEY `FK_PREREGISTROS_CICLOS` (`id_ciclo`),
  CONSTRAINT `FK_PREREGISTROS_ALUMNOS` FOREIGN KEY (`id_alumno`) REFERENCES `ALUMNOS` (`id_alumno`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla secciones.PREREGISTROS-MATERIAS
CREATE TABLE IF NOT EXISTS `PREREGISTROS-MATERIAS` (
  `id_preregistro` int(11) NOT NULL,
  `id_materia` int(11) NOT NULL,
  `situacion` enum('Primera vez','Repetidor','Art 33','Art 34','Art 35') NOT NULL DEFAULT 'Primera vez',
  `horario` enum('Matutino','Vespertino','Sabatino','Cualquiera') NOT NULL DEFAULT 'Matutino',
  KEY `FK_PREREGISTROS-MATERIAS_PREREGISTROS` (`id_preregistro`),
  KEY `FK_PREREGISTROS-MATERIAS_MATERIAS` (`id_materia`),
  CONSTRAINT `FK_PREREGISTROS-MATERIAS_PREREGISTROS` FOREIGN KEY (`id_preregistro`) REFERENCES `PREREGISTROS` (`id_preregistro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla secciones.PROFESORES
CREATE TABLE IF NOT EXISTS `PROFESORES` (
  `id_profesor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `asignatura` enum('A','B') DEFAULT NULL,
  `tipo` enum('titular','asociado','auxiliar') DEFAULT NULL,
  `investigador` bit(1) DEFAULT NULL,
  `grado` varchar(50) DEFAULT NULL,
  `tiempo` enum('completo','medio') DEFAULT NULL,
  `id_departamento` int(11) NOT NULL,
  KEY `PK_PROFESORES` (`id_profesor`),
  KEY `FK_PROFESORES_DEPARTAMENTOS` (`id_departamento`),
  CONSTRAINT `FK_PROFESORES_DEPARTAMENTOS` FOREIGN KEY (`id_departamento`) REFERENCES `DEPARTAMENTOS` (`id_departamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla secciones.PROGRAMAS
CREATE TABLE IF NOT EXISTS `PROGRAMAS` (
  `id_programa` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(10) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `id_departamento` int(11) NOT NULL,
  `preregistro_activo` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Indica el si esta activo para pre-registro',
  KEY `PK_PROGRAMAS` (`id_programa`),
  KEY `FK_PROGRAMAS_DEPARTAMENTOS` (`id_departamento`),
  KEY `IDX_CLAVE` (`clave`),
  CONSTRAINT `FK_PROGRAMAS_DEPARTAMENTOS` FOREIGN KEY (`id_departamento`) REFERENCES `DEPARTAMENTOS` (`id_departamento`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8 COMMENT='Programa por carrera';

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla secciones.PROGRAMAS-MATERIAS
CREATE TABLE IF NOT EXISTS `PROGRAMAS-MATERIAS` (
  `id_programa` int(11) DEFAULT NULL,
  `id_materia` int(11) DEFAULT NULL,
  KEY `FK_PROGRAMAS-MATERIAS_PROGRAMAS` (`id_programa`),
  KEY `FK_PROGRAMAS-MATERIAS_MATERIAS` (`id_materia`),
  CONSTRAINT `FK_PROGRAMAS-MATERIAS_PROGRAMAS` FOREIGN KEY (`id_programa`) REFERENCES `PROGRAMAS` (`id_programa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla secciones.SALONES
CREATE TABLE IF NOT EXISTS `SALONES` (
  `id_salon` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `edificio` varchar(10) DEFAULT NULL,
  `capacidad` int(11) DEFAULT NULL,
  KEY `PK_SALONES` (`id_salon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla secciones.SECCIONES
CREATE TABLE IF NOT EXISTS `SECCIONES` (
  `id_seccion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `nrc` int(11) DEFAULT NULL,
  `cupos` int(11) DEFAULT NULL,
  `id_profesor` int(11) NOT NULL,
  `id_ciclo` int(11) NOT NULL,
  `id_salon` int(11) NOT NULL,
  `id_materia` int(11) NOT NULL,
  KEY `FK_SECCIONES_CICLOS` (`id_ciclo`),
  KEY `PK_SECCIONES` (`id_seccion`),
  KEY `FK_SECCIONES_SALONES` (`id_salon`),
  KEY `FK_SECCIONES_PROFESORES` (`id_profesor`),
  KEY `FK_SECCIONES_MATERIAS` (`id_materia`),
  CONSTRAINT `FK_SECCIONES_PROFESORES` FOREIGN KEY (`id_profesor`) REFERENCES `PROFESORES` (`id_profesor`),
  CONSTRAINT `FK_SECCIONES_SALONES` FOREIGN KEY (`id_salon`) REFERENCES `SALONES` (`id_salon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla secciones.USUARIOS
CREATE TABLE IF NOT EXISTS `USUARIOS` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `puesto` varchar(100) DEFAULT NULL,
  `contrasena` varchar(64) DEFAULT NULL,
  `role` enum('admin','user') DEFAULT NULL,
  KEY `PK_USUARIOS` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
